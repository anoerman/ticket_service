/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.11 : Database - is_ticket_service_v3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`is_ticket_service_v3` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `is_ticket_service_v3`;

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`id`,`ip_address`,`timestamp`,`data`) values ('bcd1574851aac0c012abb65005ddfc73935b6c28','::1',1487664007,'notification|i:29;__ci_last_regenerate|i:1487663672;identity|s:13:\"administrator\";username|s:13:\"administrator\";email|s:15:\"admin@admin.com\";user_id|s:1:\"1\";old_last_login|s:10:\"1487663613\";');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`description`) values (1,'admin','Administrator'),(2,'members','General User');

/*Table structure for table `login_attempts` */

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `login_attempts` */

/*Table structure for table `master_gedung` */

DROP TABLE IF EXISTS `master_gedung`;

CREATE TABLE `master_gedung` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '1',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `master_gedung` */

insert  into `master_gedung`(`id`,`nama`,`aktif`,`hapus`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,'Home T',1,0,'administrator',1487042283,'administrator',1487042283),(2,'Perpustakaan',1,0,'administrator',1487042298,'administrator',1487042298),(3,'PGTK SD',1,0,'administrator',1487042313,'administrator',1487042313),(4,'SMP CB',1,0,'administrator',1487042319,'administrator',1487042319),(5,'SMA CB',1,0,'administrator',1487042324,'administrator',1487042324),(6,'SMK CB',1,0,'administrator',1487042329,'administrator',1487042329),(7,'SMP BTR',1,0,'administrator',1487042335,'administrator',1487042335),(8,'SMA BTR',1,0,'administrator',1487042340,'administrator',1487042340),(9,'Yayasan',1,0,'administrator',1487042346,'administrator',1487042346),(10,'Aula',1,0,'administrator',1487042351,'administrator',1487042351);

/*Table structure for table `master_jabatan` */

DROP TABLE IF EXISTS `master_jabatan`;

CREATE TABLE `master_jabatan` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '1',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `master_jabatan` */

insert  into `master_jabatan`(`id`,`nama`,`aktif`,`hapus`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,'Wakil Ketua Yayasan 1',1,0,'administrator',1486973570,'administrator',1486980236),(2,'Wakil Ketua Yayasan 2',1,0,'administrator',1486973913,'administrator',1486973913),(3,'Wakil Ketua Yayasan 3',1,0,'administrator',1486973975,'administrator',1486980300),(4,'Kepala Bidang Pendidikan',1,0,'administrator',1486974157,'administrator',1486974157),(5,'Kepala Bidang Keuangan',1,0,'administrator',1486975556,'administrator',1486975556),(6,'Kepala Bidang HRD',1,0,'administrator',1486975810,'administrator',1486975810),(7,'Kepala Bidang Sarana Prasarana',1,0,'administrator',1486975909,'administrator',1486975909),(8,'Kepala Bidang Umum',1,0,'administrator',1486975983,'administrator',1486975983),(9,'Kepala Bagian IT',1,0,'administrator',1486976121,'administrator',1486976121),(10,'Kepala Bagian Logistik',1,0,'administrator',1486976268,'administrator',1486976268),(11,'Kepala Bagian Humas',1,0,'administrator',1486976507,'administrator',1486976507),(12,'Kepala Bagian HRD',1,0,'administrator',1486976585,'administrator',1486976585),(13,'Kepala Bagian Sarpra',1,0,'administrator',1486976676,'administrator',1486976676),(14,'Kepala Bagian Litbang',1,0,'administrator',1486976746,'administrator',1486976746),(15,'Kepala Bagian Keuangan',1,0,'administrator',1486976868,'administrator',1486976868),(16,'Kepala Bagian Penerimaan Keuangan',1,0,'administrator',1486977017,'administrator',1486977017),(17,'Kepala Bagian Pengeluaran Keuangan',1,0,'administrator',1486977028,'administrator',1486977028),(18,'Kepala Sekolah',1,0,'administrator',1486977075,'administrator',1486977075),(19,'Wakasek Kurikulum',1,0,'administrator',1486977081,'administrator',1486977081),(20,'Wakasek Kesiswaan',1,0,'administrator',1486977086,'administrator',1486977086),(21,'Wakasek Humas',1,0,'administrator',1486977093,'administrator',1486977093),(22,'Wakabid',1,0,'administrator',1486977100,'administrator',1486977100),(23,'Wakabag',1,0,'administrator',1486977106,'administrator',1486977106),(24,'Staf',1,0,'administrator',1486977112,'administrator',1486977112),(25,'Guru',1,0,'administrator',1486977119,'administrator',1486977119),(26,'Tata Usaha',1,0,'administrator',1486977125,'administrator',1486977125);

/*Table structure for table `master_jenis_tiket` */

DROP TABLE IF EXISTS `master_jenis_tiket`;

CREATE TABLE `master_jenis_tiket` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '1',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `master_jenis_tiket` */

insert  into `master_jenis_tiket`(`id`,`nama`,`aktif`,`hapus`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,'Printer',1,0,'administrator',1487066255,'administrator',1487066255),(2,'Komputer',1,0,'administrator',1487066264,'administrator',1487066264),(3,'Jaringan',1,0,'administrator',1487066268,'administrator',1487066268),(4,'Server',1,0,'administrator',1487066271,'administrator',1487066271);

/*Table structure for table `master_lokasi_kerja` */

DROP TABLE IF EXISTS `master_lokasi_kerja`;

CREATE TABLE `master_lokasi_kerja` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '1',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `master_lokasi_kerja` */

insert  into `master_lokasi_kerja`(`id`,`nama`,`aktif`,`hapus`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,'Yayasan Cakrabuana',1,0,'administrator',1487065450,'administrator',1487065450),(2,'Yayasan Bintara',1,0,'administrator',1487065962,'administrator',1487065962),(3,'Yayasan Cakrabuana Bintara',1,0,'administrator',1487065974,'administrator',1487065974);

/*Table structure for table `master_ruang` */

DROP TABLE IF EXISTS `master_ruang`;

CREATE TABLE `master_ruang` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '1',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `master_ruang` */

insert  into `master_ruang`(`id`,`nama`,`aktif`,`hapus`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,'IT',1,0,'administrator',1487042849,'administrator',1487042960),(2,'Acc Yayasan',1,0,'administrator',1487042857,'administrator',1487042857),(3,'HRD',1,0,'administrator',1487042862,'administrator',1487042862),(4,'Humas',1,0,'administrator',1487042867,'administrator',1487042867),(5,'Kabag',1,0,'administrator',1487042872,'administrator',1487042872),(6,'Keuangan',1,0,'administrator',1487042877,'administrator',1487042877),(7,'Lab SD',1,0,'administrator',1487042883,'administrator',1487042887),(8,'Logistik',1,0,'administrator',1487042892,'administrator',1487042892),(9,'Pendidikan',1,0,'administrator',1487042896,'administrator',1487042896),(10,'Perpustakaan',1,0,'administrator',1487042900,'administrator',1487042900),(11,'Ruang Guru',1,0,'administrator',1487042905,'administrator',1487042905),(12,'Ruang Kepsek',1,0,'administrator',1487042909,'administrator',1487042909),(13,'Sarpra',1,0,'administrator',1487042913,'administrator',1487042913),(14,'Seragam',1,0,'administrator',1487042917,'administrator',1487042917),(15,'TU',1,0,'administrator',1487042921,'administrator',1487042921),(16,'Research',1,0,'administrator',1487042928,'administrator',1487042928),(17,'Kelas',1,0,'administrator',1487042936,'administrator',1487042936),(18,'Pantry',1,0,'administrator',1487042940,'administrator',1487042940),(19,'Koridor',1,0,'administrator',1487042945,'administrator',1487042945);

/*Table structure for table `master_status_pegawai` */

DROP TABLE IF EXISTS `master_status_pegawai`;

CREATE TABLE `master_status_pegawai` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '1',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `master_status_pegawai` */

insert  into `master_status_pegawai`(`id`,`nama`,`aktif`,`hapus`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,'Training',1,0,'administrator',1487041071,'administrator',1487041158),(2,'Karyawan Yayasan',1,0,'administrator',1487041108,'administrator',1487041108),(3,'Karyawan Tetap Yayasan',1,0,'administrator',1487041123,'administrator',1487041123),(4,'Guru Tidak Tetap',1,0,'administrator',1487041133,'administrator',1487041133),(5,'Guru Tetap Yayasan',1,0,'administrator',1487041143,'administrator',1487041143);

/*Table structure for table `master_unit_kerja` */

DROP TABLE IF EXISTS `master_unit_kerja`;

CREATE TABLE `master_unit_kerja` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '1',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `master_unit_kerja` */

insert  into `master_unit_kerja`(`id`,`nama`,`aktif`,`hapus`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,'PGTK CB',1,0,'administrator',1487041733,'administrator',1487041733),(2,'SD CB',1,0,'administrator',1487041741,'administrator',1487041741),(3,'SMP CB',1,0,'administrator',1487041746,'administrator',1487041746),(4,'SMA CB',1,0,'administrator',1487041753,'administrator',1487041753),(5,'SMK CB',1,0,'administrator',1487041759,'administrator',1487041759),(6,'SMP BTR',1,0,'administrator',1487041766,'administrator',1487041766),(7,'SMA BTR',1,0,'administrator',1487041771,'administrator',1487041771),(8,'IT',1,0,'administrator',1487041778,'administrator',1487041778),(9,'Logistik',1,0,'administrator',1487041786,'administrator',1487041786),(10,'Humas',1,0,'administrator',1487041793,'administrator',1487041793),(11,'HRD',1,0,'administrator',1487041804,'administrator',1487041804),(12,'Yayasan BTR CB',1,0,'administrator',1487041854,'administrator',1487041854),(13,'Keuangan',1,0,'administrator',1487041863,'administrator',1487041863),(14,'Kependidikan',1,0,'administrator',1487041869,'administrator',1487041869),(15,'Sarpra',1,0,'administrator',1487041874,'administrator',1487041874),(16,'Perpustakaan',1,0,'administrator',1487041885,'administrator',1487041885),(17,'Paramedis',1,0,'administrator',1487041891,'administrator',1487041891),(18,'Umum',1,0,'administrator',1487041898,'administrator',1487041898),(19,'Litbang',1,0,'administrator',1487041904,'administrator',1487041904);

/*Table structure for table `tiket` */

DROP TABLE IF EXISTS `tiket`;

CREATE TABLE `tiket` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `jenis_tiket_id` int(12) NOT NULL,
  `tanggal` int(10) NOT NULL,
  `oleh` varchar(100) NOT NULL,
  `isi` text,
  `status` enum('daftar tunggu','proses','tertunda','selesai','tidak selesai','batal') NOT NULL DEFAULT 'daftar tunggu',
  `hapus` tinyint(1) NOT NULL DEFAULT '0',
  `selesai_oleh` varchar(100) NOT NULL,
  `selesai_tanggal` int(10) NOT NULL,
  `penanganan` text,
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `tiket` */

insert  into `tiket`(`id`,`kode`,`jenis_tiket_id`,`tanggal`,`oleh`,`isi`,`status`,`hapus`,`selesai_oleh`,`selesai_tanggal`,`penanganan`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,'BCB.IT.170220.TN.1',1,1487555624,'120.215.1.2159','1','daftar tunggu',0,'',0,NULL,'120.215.1.2159',1487555624,'120.215.1.2159',1487555624),(2,'BCB.IT.170220.TN.2',3,1487565550,'administrator','2','daftar tunggu',0,'',0,NULL,'administrator',1487565550,'administrator',1487565550),(3,'BCB.IT.170220.TN.3',4,1487575562,'administrator','3','daftar tunggu',0,'',0,NULL,'administrator',1487575562,'administrator',1487575562),(4,'BCB.IT.170220.TN.4',1,1487575599,'administrator','4','daftar tunggu',0,'',0,NULL,'administrator',1487575599,'administrator',1487575599),(5,'BCB.IT.170221.TN.5',1,1487647313,'administrator','5','daftar tunggu',0,'',0,NULL,'administrator',1487647313,'administrator',1487647313),(6,'BCB.IT.170221.TN.6',2,1487650971,'administrator','6','daftar tunggu',0,'',0,NULL,'administrator',1487650971,'administrator',1487650971),(7,'BCB.IT.170221.TN.7',4,1487659367,'administrator','Cobain yang 7 ini sekarang!','daftar tunggu',0,'',0,NULL,'administrator',1487659367,'administrator',1487659367),(8,'BCB.IT.170221.TN.8',1,1487659632,'administrator','Next!!! 8','daftar tunggu',0,'',0,NULL,'administrator',1487659632,'administrator',1487659632),(9,'BCB.IT.170221.TN.9',1,1487659746,'administrator','please 9','daftar tunggu',0,'',0,NULL,'administrator',1487659746,'administrator',1487659746),(10,'BCB.IT.170221.TN.10',3,1487659792,'administrator','tring 10','daftar tunggu',0,'',0,NULL,'administrator',1487659792,'administrator',1487659792),(11,'BCB.IT.170221.TN.11',3,1487660060,'administrator','tring tring 11','daftar tunggu',0,'',0,NULL,'administrator',1487660060,'administrator',1487660060),(12,'BCB.IT.170221.TN.12',3,1487660469,'administrator','tring tring tring 12','daftar tunggu',0,'',0,NULL,'administrator',1487660469,'administrator',1487660469),(13,'BCB.IT.170221.TN.13',2,1487660784,'administrator','Aaaaaaaahhhhhh 13 haaaaa','daftar tunggu',0,'',0,NULL,'administrator',1487660784,'administrator',1487660784),(14,'BCB.IT.170221.TN.14',1,1487660945,'administrator','14 Sugar','daftar tunggu',0,'',0,NULL,'administrator',1487660945,'administrator',1487660945),(15,'BCB.IT.170221.TN.15',2,1487660979,'administrator','15 next','daftar tunggu',0,'',0,NULL,'administrator',1487660979,'administrator',1487660979),(16,'BCB.IT.170221.TN.16',1,1487661051,'administrator','in my 16 life','daftar tunggu',0,'',0,NULL,'administrator',1487661051,'administrator',1487661051),(17,'BCB.IT.170221.TN.17',2,1487661220,'administrator','tes ke 17','daftar tunggu',0,'',0,NULL,'administrator',1487661220,'administrator',1487661220),(18,'BCB.IT.170221.TN.18',1,1487661318,'administrator','shake this 18 off','daftar tunggu',0,'',0,NULL,'administrator',1487661318,'administrator',1487661318),(19,'BCB.IT.170221.TN.19',1,1487661508,'administrator','scared of 19','daftar tunggu',0,'',0,NULL,'administrator',1487661508,'administrator',1487661508),(20,'BCB.IT.170221.TN.20',1,1487661534,'administrator','20th tests','daftar tunggu',0,'',0,NULL,'administrator',1487661534,'administrator',1487661534),(21,'BCB.IT.170221.TN.21',3,1487661850,'administrator','now that i found you 21','daftar tunggu',0,'',0,NULL,'administrator',1487661850,'administrator',1487661850),(22,'BCB.IT.170221.TN.22',2,1487661933,'administrator','never give up on 22!!!','daftar tunggu',0,'',0,NULL,'administrator',1487661933,'administrator',1487661933),(23,'BCB.IT.170221.TN.23',1,1487662063,'administrator','yap no 23','daftar tunggu',0,'',0,NULL,'administrator',1487662063,'administrator',1487662063),(24,'BCB.IT.170221.TN.24',2,1487662181,'administrator','24 ahaha','daftar tunggu',0,'',0,NULL,'administrator',1487662181,'administrator',1487662181),(25,'BCB.IT.170221.TN.25',1,1487662907,'administrator','25','daftar tunggu',0,'',0,NULL,'administrator',1487662907,'administrator',1487662907),(26,'BCB.IT.170221.TN.26',3,1487662975,'administrator','26','daftar tunggu',0,'',0,NULL,'administrator',1487662975,'administrator',1487662975),(27,'BCB.IT.170221.TN.27',1,1487663090,'administrator','27','daftar tunggu',0,'',0,NULL,'administrator',1487663090,'administrator',1487663090),(28,'BCB.IT.170221.TN.28',3,1487663206,'administrator','28','daftar tunggu',0,'',0,NULL,'administrator',1487663206,'administrator',1487663206),(29,'BCB.IT.170221.TN.29',2,1487663723,'administrator','29','daftar tunggu',0,'',0,NULL,'administrator',1487663723,'administrator',1487663723);

/*Table structure for table `tiket_detail` */

DROP TABLE IF EXISTS `tiket_detail`;

CREATE TABLE `tiket_detail` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `tiket_id` int(15) NOT NULL,
  `jabatan_id` int(12) NOT NULL,
  `status_pegawai_id` int(12) NOT NULL,
  `unit_kerja_id` int(12) NOT NULL,
  `lokasi_kerja_id` int(12) NOT NULL,
  `gedung_id` int(12) NOT NULL,
  `ruang_id` int(12) NOT NULL,
  `lantai` int(12) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` int(10) NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `updated_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `tiket_detail` */

insert  into `tiket_detail`(`id`,`tiket_id`,`jabatan_id`,`status_pegawai_id`,`unit_kerja_id`,`lokasi_kerja_id`,`gedung_id`,`ruang_id`,`lantai`,`created_by`,`created_on`,`updated_by`,`updated_on`) values (1,1,24,3,8,3,1,1,2,'120.215.1.2159',1487555624,'120.215.1.2159',1487555624),(2,2,24,3,8,3,1,1,2,'administrator',1487565550,'administrator',1487565550),(3,3,24,3,8,3,1,1,2,'administrator',1487575562,'administrator',1487575562),(4,4,24,3,8,3,1,1,2,'administrator',1487575599,'administrator',1487575599),(5,5,24,3,8,3,1,1,2,'administrator',1487647313,'administrator',1487647313),(6,6,24,3,8,3,1,1,2,'administrator',1487650971,'administrator',1487650971),(7,7,24,3,8,3,1,4,2,'administrator',1487659367,'administrator',1487659367),(8,8,24,3,8,3,3,1,2,'administrator',1487659632,'administrator',1487659632),(9,9,24,3,8,3,1,1,2,'administrator',1487659746,'administrator',1487659746),(10,10,24,3,8,3,1,1,2,'administrator',1487659792,'administrator',1487659792),(11,11,24,3,8,3,1,1,2,'administrator',1487660060,'administrator',1487660060),(12,12,24,3,8,3,1,1,2,'administrator',1487660469,'administrator',1487660469),(13,13,24,3,8,3,1,1,2,'administrator',1487660784,'administrator',1487660784),(14,14,24,3,8,3,1,1,2,'administrator',1487660945,'administrator',1487660945),(15,15,24,3,8,3,1,1,2,'administrator',1487660979,'administrator',1487660979),(16,16,24,3,8,3,1,1,2,'administrator',1487661051,'administrator',1487661051),(17,17,24,3,8,3,1,1,2,'administrator',1487661220,'administrator',1487661220),(18,18,24,3,8,3,1,1,2,'administrator',1487661318,'administrator',1487661318),(19,19,24,3,8,3,1,1,2,'administrator',1487661508,'administrator',1487661508),(20,20,24,3,8,3,1,1,2,'administrator',1487661534,'administrator',1487661534),(21,21,24,3,8,3,1,1,2,'administrator',1487661850,'administrator',1487661850),(22,22,24,3,8,3,1,1,2,'administrator',1487661933,'administrator',1487661933),(23,23,24,3,8,3,1,1,2,'administrator',1487662063,'administrator',1487662063),(24,24,24,3,8,3,1,1,2,'administrator',1487662181,'administrator',1487662181),(25,25,24,3,8,3,1,1,2,'administrator',1487662907,'administrator',1487662907),(26,26,24,3,8,3,1,1,2,'administrator',1487662975,'administrator',1487662975),(27,27,24,3,8,3,6,3,2,'administrator',1487663090,'administrator',1487663090),(28,28,24,3,8,3,1,1,2,'administrator',1487663206,'administrator',1487663206),(29,29,24,3,8,2,2,2,2,'administrator',1487663723,'administrator',1487663723);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`ip_address`,`username`,`password`,`salt`,`email`,`activation_code`,`forgotten_password_code`,`forgotten_password_time`,`remember_code`,`created_on`,`last_login`,`active`,`first_name`,`last_name`) values (1,'127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com','8ed9418da422a557a901801666340d2836c44293','aDPWtcK1U5jG2SCSjcoJ-O9e3616d4b6c5314db0',1486002907,NULL,1268889823,1487663693,1,'Admin','istrator'),(5,'::1','120.215.1.2169','$2y$08$tJbOfhKYvCS6VOUKNtUbVencXo8orrNMWYmoqiMvg2sczvmCqMRAC',NULL,'noerman.agustiyan@gmail.com',NULL,NULL,NULL,NULL,1487214553,1487215169,1,'Noerman','Agustiyan'),(6,'::1','120.215.1.2159','$2y$08$yCCYXd7KOQdCqB4SO.V27OCSacR6rwbVIibF7mbJ4.jZUq62Iu4Zm',NULL,'',NULL,NULL,NULL,NULL,1487214901,1487555586,1,'Andri','Wahyu Nugroho');

/*Table structure for table `users_gedung` */

DROP TABLE IF EXISTS `users_gedung`;

CREATE TABLE `users_gedung` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `gedung_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `users_gedung` */

insert  into `users_gedung`(`id`,`user_id`,`gedung_id`) values (1,5,1),(2,6,1),(3,1,1);

/*Table structure for table `users_groups` */

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `users_groups` */

insert  into `users_groups`(`id`,`user_id`,`group_id`) values (28,1,1),(29,1,2),(26,5,1),(27,5,2),(25,6,2);

/*Table structure for table `users_jabatan` */

DROP TABLE IF EXISTS `users_jabatan`;

CREATE TABLE `users_jabatan` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `jabatan_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `users_jabatan` */

insert  into `users_jabatan`(`id`,`user_id`,`jabatan_id`) values (1,5,24),(2,6,24),(3,1,24);

/*Table structure for table `users_lantai` */

DROP TABLE IF EXISTS `users_lantai`;

CREATE TABLE `users_lantai` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `lantai` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `users_lantai` */

insert  into `users_lantai`(`id`,`user_id`,`lantai`) values (1,5,2),(2,6,2),(3,1,2);

/*Table structure for table `users_lokasi_kerja` */

DROP TABLE IF EXISTS `users_lokasi_kerja`;

CREATE TABLE `users_lokasi_kerja` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `lokasi_kerja_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `users_lokasi_kerja` */

insert  into `users_lokasi_kerja`(`id`,`user_id`,`lokasi_kerja_id`) values (1,5,1),(2,6,3),(3,1,3);

/*Table structure for table `users_ruang` */

DROP TABLE IF EXISTS `users_ruang`;

CREATE TABLE `users_ruang` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `ruang_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `users_ruang` */

insert  into `users_ruang`(`id`,`user_id`,`ruang_id`) values (1,5,1),(2,6,1),(3,1,1);

/*Table structure for table `users_status_pegawai` */

DROP TABLE IF EXISTS `users_status_pegawai`;

CREATE TABLE `users_status_pegawai` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `status_pegawai_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `users_status_pegawai` */

insert  into `users_status_pegawai`(`id`,`user_id`,`status_pegawai_id`) values (1,5,3),(2,6,3),(3,1,3);

/*Table structure for table `users_unit_kerja` */

DROP TABLE IF EXISTS `users_unit_kerja`;

CREATE TABLE `users_unit_kerja` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `unit_kerja_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `users_unit_kerja` */

insert  into `users_unit_kerja`(`id`,`user_id`,`unit_kerja_id`) values (1,5,8),(2,6,8),(3,1,8);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
