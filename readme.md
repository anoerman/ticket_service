# IS Service Ticket V3
Aplikasi tiket servis untuk tracking dan pendataan permasalahan IT Versi 3.

## Sinopsis
Tidak jarang berbagai masalah pada komputer atau jaringan terjadi pada sebuah instansi atau perusahaan, semakin besar instansi atau perusahaan tersebut maka akan semakin banyak kemungkinan terjadi masalah. Pendataan masalah inilah yang menjadi sorotan utama pada aplikasi ini. 
Pada aplikasi ini nantinya semua masalah tersebut dapat di sortir berdasarkan jenis masalah dan status penanganan masalah tersebut. Nantinya aplikasi ini juga dapat melakukan pembuatan laporan dari semua tiket yang telah masuk. Kelebihan yang didapatkan nantinya adalah semua data permasalahan dapat lebih teratur, pembuatan laporan dapat lebih mudah dan pada akhirnya pihak IT dapat melakukan pembuatan statistik mengenai kinerja karyawannya (terutama bagian IT) dari segi penyelesaian masalah yang terjadi.

## Instalasi
* Download atau Clone aplikasi.
* Import file SQL (is_ticket_service_v3.sql).
* Ubah file config pada folder config sesuai kebutuhan.
* Ubah file database pada folder config sesuai kebutuhan.
* Ubah file site pada folder config untuk mengubah nama dan slogan aplikasi

## Login
* Username : administrator
* Password : password

## License
Aplikasi ini memiliki lisensi MIT.