<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
*	Tiket Controller
*	
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*	
*	Controller semua kegiatan yang terjadi pada halaman tiket
*	Hanya dapat diakses oleh user dengan group admin
*	Melakukan pengaturan terhadap data yang berhubungan dengan
*	tiket seperti pembuatan, perubahan, penghapusan dan juga 
*	menampilkan data tiket yang ada di dalam database.
*	
*/
class Tiket extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set error delimeters
		$this->form_validation->set_error_delimiters(
			$this->config->item('error_start_delimiter', 'ion_auth'), 
			$this->config->item('error_end_delimiter', 'ion_auth')
		);

		// set delimeters
		$this->message_start_delimiter = $this->config->item('message_start_delimiter', 'ion_auth');
		$this->message_end_delimiter   = $this->config->item('message_end_delimiter', 'ion_auth');
		$this->error_start_delimiter   = $this->config->item('error_start_delimiter', 'ion_auth');
		$this->error_end_delimiter     = $this->config->item('error_end_delimiter', 'ion_auth');

		// load site config
		$this->load->config('site', TRUE);

		// set default setting based on site config
		$this->data['site_title']  = $this->config->item('site_title', 'site');
		$this->data['site_slogan'] = $this->config->item('site_slogan', 'site');
		$this->data['page']        = "tiket";

		// load model
		$this->load->model(array('tiket_model', 'pengguna_tiket_model'));

		// pagination
		$this->load->library(array('pagination'));
		$this->load->config('pagination');
	}

	/**
	*	Index
	*	Menampilkan statistik tiket
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-17)
	*	
	*	@return 		void
	*	
	*/
	public function index()
	{
		// if (!$this->ion_auth->logged_in()) {
		// 	redirect('auth/login', 'refresh');
		// }
		// else {
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			
			// Set userdata notification sesuai total tiket
			$this->session->set_userdata(
				'notification', 
				count($this->tiket_model->ambil_tiket('daftar tunggu')->result())
			);
			// Waktu refresh halaman dalam milisecond
			// (standar 30 detik)
			$this->data['time_refresh'] = 30000; 

			if (!$this->ion_auth->logged_in()) {
				$this->load->view('template/header', $this->data);
			}
			else {
				if (!$this->ion_auth->is_admin()) {
					$this->load->view('template/auth_header', $this->data);
				}
				// header admin
				else {
					$this->load->view('template/auth_admin_header', $this->data);
				}
			}
			$this->load->view('ticket/index');
			$this->load->view('template/auth_footer');
			$this->load->view('ticket/index.js');
		// }
	}

	/**
	*	Statistik
	*	Menampilkan statistik tiket
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-17)
	*	
	*	@return 		void
	*	
	*/
	public function statistik()
	{
		// set the flash data error message if there is one
		$this->data['message']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket('daftar tunggu');
		// pengaturan untuk notif suara jika ada tiket baru
		$this->data['sound_path'] = base_url('assets/sound/youve-been-informed'); // dan nama file sound tanpa extension

		// setting suara notif
		$total_tiket = count($this->data['data_tiket']->result());

		$notification_flash = $this->session->userdata('notification');
		// Jika userdata notification kosong
		if ($notification_flash !== null) {
			if ($notification_flash == $total_tiket) {
				$this->data['notification'] = 'no';
			}
			elseif ($notification_flash < $total_tiket) {
				$this->data['notification'] = 'yes';
			}
		}
		else {
			$this->data['notification'] = 'no';
		}

		$this->data['total_tiket'] = $total_tiket;

		// data kiriman untuk chart keseluruhan
		$this->data['n_daftar_tunggu'] = $this->tiket_model->hitung_tiket('daftar tunggu');
		$this->data['n_proses']        = $this->tiket_model->hitung_tiket('proses');
		$this->data['n_tertunda']      = $this->tiket_model->hitung_tiket('tertunda');
		$this->data['n_tidak_selesai'] = $this->tiket_model->hitung_tiket('tidak selesai');
		$this->data['n_selesai']       = $this->tiket_model->hitung_tiket('selesai');

		// data kiriman untuk chart 7 hari terakhir
		// tanggal sekarang
		$this->data['hari_ke_6'] = strtotime((new DateTime('6 days ago'))->format('Y-m-d'));
		$this->data['hari_ke_5'] = strtotime((new DateTime('5 days ago'))->format('Y-m-d'));
		$this->data['hari_ke_4'] = strtotime((new DateTime('4 days ago'))->format('Y-m-d'));
		$this->data['hari_ke_3'] = strtotime((new DateTime('3 days ago'))->format('Y-m-d'));
		$this->data['hari_ke_2'] = strtotime((new DateTime('2 days ago'))->format('Y-m-d'));
		$this->data['hari_ke_1'] = strtotime((new DateTime('1 days ago'))->format('Y-m-d'));
		$this->data['sekarang']  = strtotime(date('Y-m-d', time()));
		// hasil per tanggal
		$this->data['n_hari_ke_6'] = $this->tiket_model->hitung_tiket_per_tanggal($this->data['hari_ke_6']);
		$this->data['n_hari_ke_5'] = $this->tiket_model->hitung_tiket_per_tanggal($this->data['hari_ke_5']);
		$this->data['n_hari_ke_4'] = $this->tiket_model->hitung_tiket_per_tanggal($this->data['hari_ke_4']);
		$this->data['n_hari_ke_3'] = $this->tiket_model->hitung_tiket_per_tanggal($this->data['hari_ke_3']);
		$this->data['n_hari_ke_2'] = $this->tiket_model->hitung_tiket_per_tanggal($this->data['hari_ke_2']);
		$this->data['n_hari_ke_1'] = $this->tiket_model->hitung_tiket_per_tanggal($this->data['hari_ke_1']);
		$this->data['n_sekarang']  = $this->tiket_model->hitung_tiket_per_tanggal($this->data['sekarang']);

		$this->load->view('ticket/index_chart', $this->data);
	}

	/**
	*	Kode
	*	Membuat kode tiket baru
	*	Anda dapat melakukan perubahan format kode
	*	sesuai dengan keinginan
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-18)
	*	
	*	@return 		string 		$kode
	*	
	*/
	public function kode()
	{
		// Ambil max id dari tabel tiket
		$this->db->select_max('id', 'max_id');
		$query = $this->db->get('tiket');
		$new_id = $query->row()->max_id + 1;

		$kode = "BCB.IT." . date('ymd.') . "TN." . $new_id;

		return $kode;
	}

	/**
	*	Tambah
	*	Mengakses halaman tambah tiket atau melakukan penyimpanan
	*	data tiket yang telah dimasukkan.
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.1 (2017-02-21)
	*	
	*	@return 		void
	*	
	*/
	public function tambah()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		else {
			// validate form input
			$this->form_validation->set_rules('jenis_tiket', 'Jenis Tiket', 'required');
			$this->form_validation->set_rules('isi', 'Isi Tiket', 'required|addslashes|trim');
			
			// jika validasi berhasil jalankan proses simpan tiket
			if ($this->form_validation->run() == true) {
				$datas = array(
					'kode'           => $this->kode(),
					'jenis_tiket_id' => $this->input->post('jenis_tiket'),
					'tanggal'        => time(),
					'oleh'           => $this->input->post('oleh'),
					'isi'            => $this->input->post('isi'),
					'status'         => 'daftar tunggu',
					'created_by'     => $this->session->userdata('identity'),
					'created_on'     => time(),
					'updated_by'     => $this->session->userdata('identity'),
					'updated_on'     => time(),
				);
				if ($this->tiket_model->simpan($datas)) {
					// simpan detail
					$tiket_id = $this->db->insert_id();
					$datas = array(
						'tiket_id'          => $tiket_id,
						'jabatan_id'        => $this->input->post('jabatan_id'),
						'status_pegawai_id' => $this->input->post('status_pegawai_id'),
						'unit_kerja_id'     => $this->input->post('unit_kerja_id'),
						'lokasi_kerja_id'   => $this->input->post('lokasi_kerja_id'),
						'gedung_id'         => $this->input->post('gedung_id'),
						'ruang_id'          => $this->input->post('ruang_id'),
						'lantai'            => $this->input->post('lantai'),
						'created_by'        => $this->session->userdata('identity'),
						'created_on'        => time(),
						'updated_by'        => $this->session->userdata('identity'),
						'updated_on'        => time(),
					);

					if ($this->tiket_model->simpan_detail($datas)) {
						$this->session->set_flashdata('message', $this->message_start_delimiter.'Tiket berhasil dibuat!'.$this->message_end_delimiter);
					}
					else {
						$this->session->set_flashdata('message', $this->message_start_delimiter.'Detail Tiket gagal dibuat!'.$this->message_end_delimiter);
					}

				}
				else {
					$this->session->set_flashdata('message', $this->message_start_delimiter.'Tiket gagal dibuat!'.$this->message_end_delimiter);
				}
					redirect('tiket/tambah', 'refresh');
			}
			else {
				// set the flash data error message if there is one
				$this->data['message']         = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->data['kode']            = $this->kode();
				$this->data['jenis_tiket']     = $this->tiket_model->jenis_tiket();
				$this->data['jenis_tiket_val'] = $this->input->post('jenis_tiket');
				$this->data['isi_val']         = $this->form_validation->set_value('isi');
				$this->data['user']            = $this->ion_auth->user($this->session->userdata('user_id'))->row();
				$this->detail_pengguna_tiket($this->session->userdata('user_id'));

				if (!$this->ion_auth->is_admin()) {
					$this->load->view('template/auth_header', $this->data);
				}
				// header admin
				else {
					$this->load->view('template/auth_admin_header', $this->data);
				}	
				$this->load->view('ticket/tambah');
				$this->load->view('template/auth_footer');
			}
		}
	}

	/**
	*	Detail Pengguna Tiket
	*	Memberikan detail pengguna tiket dan juga data
	*	yang dapat digunakan pada formulir input tiket
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-18)
	*	
	*	@param 			string 		$user_id
	*	@return 		array 		$this->data
	*	
	*/
	public function detail_pengguna_tiket($user_id = null)
	{
		// ambil detail yang sudah ada di database berdasarkan user_id
		$this->data['jabatan_sekarang']        = $this->pengguna_tiket_model->ambil_users_jabatan($user_id)->row();
		$this->data['status_pegawai_sekarang'] = $this->pengguna_tiket_model->ambil_users_status_pegawai($user_id)->row();
		$this->data['unit_kerja_sekarang']     = $this->pengguna_tiket_model->ambil_users_unit_kerja($user_id)->row();
		$this->data['lokasi_kerja_sekarang']   = $this->pengguna_tiket_model->ambil_users_lokasi_kerja($user_id)->row();
		$this->data['gedung_sekarang']         = $this->pengguna_tiket_model->ambil_users_gedung($user_id)->row();
		$this->data['ruang_sekarang']          = $this->pengguna_tiket_model->ambil_users_ruang($user_id)->row();
		$this->data['lantai_sekarang']         = $this->pengguna_tiket_model->ambil_users_lantai($user_id)->row();

		// data select untuk data yang dapat diubah
		$this->data['lokasi_kerja_data'] = $this->pengguna_tiket_model->lokasi_kerja();
		$this->data['gedung_data']       = $this->pengguna_tiket_model->gedung();
		$this->data['ruang_data']        = $this->pengguna_tiket_model->ruang();

		return $this->data;
	}

	/**
	*	Daftar Tunggu Tiket
	*	Menampilkan tiket yang statusnya daftar tunggu
	*	Ada sistem auto refresh dan notification sound
	*	apabila ada tiket baru yang masuk ke sistem.
	*	
	*	Setting notification :
	*	======================
	*	sound_path   = path dan nama file suara (tanpa
	*				   ekstensi)
	*	time_refresh = waktu refresh halaman otomatis
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.1.0 (2017-02-21)
	*	
	*	@return 		void
	*	
	*/
	public function daftar_tunggu()
	{
		// Set userdata notification sesuai total tiket
		$this->session->set_userdata(
			'notification', 
			count($this->tiket_model->ambil_tiket('daftar tunggu')->result())
		);
		// Waktu refresh halaman dalam milisecond
		// (standar 30 detik)
		$this->data['time_refresh'] = 30000; 
		$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		// tentukan header berdasarkan level
		if (!$this->ion_auth->logged_in()) {
			$this->load->view('template/header', $this->data);
		}
		elseif ($this->ion_auth->is_admin()) {
			// header admin
			$this->load->view('template/auth_admin_header', $this->data);
		}
		else {
			$this->load->view('template/auth_header', $this->data);
		}	
		$this->load->view('ticket/daftar_tunggu');
		$this->load->view('template/auth_footer');
		$this->load->view('ticket/daftar_tunggu.js');
	}

	public function daftar_tunggu_isi()
	{
		// set the flash data error message if there is one
		$this->data['message']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket('daftar tunggu');
		$this->data['sound_path'] = base_url('assets/sound/youve-been-informed'); // dan nama file sound tanpa extension

		// setting suara notif
		$total_tiket = count($this->data['data_tiket']->result());

		$notification_flash = $this->session->userdata('notification');
		// Jika userdata notification kosong
		if ($notification_flash !== null) {
			if ($notification_flash == $total_tiket) {
				$this->data['notification'] = 'no';
			}
			elseif ($notification_flash < $total_tiket) {
				$this->data['notification'] = 'yes';
			}
		}
		else {
			$this->data['notification'] = 'no';
		}

		$this->data['total_tiket'] = $total_tiket;

		$this->load->view('ticket/daftar_tunggu_isi', $this->data);
	}

	/**
	*	Tiket Proses
	*	Menampilkan tiket yang statusnya proses.
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.2.0 (2017-02-27)
	*	
	*	@param 			string 		$page
	*	@return 		void
	*	
	*/
	public function proses($page = null)
	{
		// set the flash data error message if there is one
		$this->data['message']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket('proses');

		// Set pagination
		$config['base_url']   = base_url('tiket/proses');
		$config['total_rows'] = count($this->data['data_tiket']->result());
		$config['per_page']   = 15;
		$this->pagination->initialize($config);

		// Ambil ulang data 
		// dan set limit halaman untuk print pagination
		if ($page=="") { $page = 0; }
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket(
			"proses",
			"",
			$config['per_page'], 
			$page
		);
		$this->data['pagination'] = $this->pagination->create_links();


		// tentukan header berdasarkan level
		if (!$this->ion_auth->logged_in()) {
			$this->load->view('template/header', $this->data);
		}
		elseif ($this->ion_auth->is_admin()) {
			// header admin
			$this->load->view('template/auth_admin_header', $this->data);
		}
		else {
			$this->load->view('template/auth_header', $this->data);
		}	
		$this->load->view('ticket/proses');
		$this->load->view('template/auth_footer');
	}

	/**
	*	Tiket tertunda
	*	Menampilkan tiket yang statusnya tertunda.
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.2.0 (2017-02-27)
	*	
	*	@param 			string 		$page
	*	@return 		void
	*	
	*/
	public function tertunda($page = null)
	{
		// set the flash data error message if there is one
		$this->data['message']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket('tertunda');

		// Set pagination
		$config['base_url']   = base_url('tiket/tertunda');
		$config['total_rows'] = count($this->data['data_tiket']->result());
		$config['per_page']   = 15;
		$this->pagination->initialize($config);

		// Ambil ulang data 
		// dan set limit halaman untuk print pagination
		if ($page=="") { $page = 0; }
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket(
			"tertunda",
			"",
			$config['per_page'], 
			$page
		);
		$this->data['pagination'] = $this->pagination->create_links();

		// tentukan header berdasarkan level
		if (!$this->ion_auth->logged_in()) {
			$this->load->view('template/header', $this->data);
		}
		elseif ($this->ion_auth->is_admin()) {
			// header admin
			$this->load->view('template/auth_admin_header', $this->data);
		}
		else {
			$this->load->view('template/auth_header', $this->data);
		}	
		$this->load->view('ticket/tertunda');
		$this->load->view('template/auth_footer');
	}

	/**
	*	Tiket tidak selesai
	*	Menampilkan tiket yang statusnya tidak selesai.
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.2.0 (2017-02-27)
	*	
	*	@param 			string 		$page
	*	@return 		void
	*	
	*/
	public function tidak_selesai($page = null)
	{
		// set the flash data error message if there is one
		$this->data['message']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket('tidak selesai');

		// Set pagination
		$config['base_url']   = base_url('tiket/tidak_selesai');
		$config['total_rows'] = count($this->data['data_tiket']->result());
		$config['per_page']   = 15;
		$this->pagination->initialize($config);

		// Ambil ulang data 
		// dan set limit halaman untuk print pagination
		if ($page=="") { $page = 0; }
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket(
			"tidak selesai",
			"",
			$config['per_page'], 
			$page
		);
		$this->data['pagination'] = $this->pagination->create_links();

		// tentukan header berdasarkan level
		if (!$this->ion_auth->logged_in()) {
			$this->load->view('template/header', $this->data);
		}
		elseif ($this->ion_auth->is_admin()) {
			// header admin
			$this->load->view('template/auth_admin_header', $this->data);
		}
		else {
			$this->load->view('template/auth_header', $this->data);
		}	
		$this->load->view('ticket/tidak_selesai');
		$this->load->view('template/auth_footer');
	}

	/**
	*	Tiket selesai
	*	Menampilkan tiket yang statusnya selesai.
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.2.0 (2017-02-27)
	*	
	*	@param 			string 		$page
	*	@return 		void
	*	
	*/
	public function selesai($page = null)
	{
		// set the flash data error message if there is one
		$this->data['message']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket('selesai');

		// Set pagination
		$config['base_url']   = base_url('tiket/selesai');
		$config['total_rows'] = count($this->data['data_tiket']->result());
		$config['per_page']   = 15;
		$this->pagination->initialize($config);

		// Ambil ulang data 
		// dan set limit halaman untuk print pagination
		if ($page=="") { $page = 0; }
		$this->data['data_tiket'] = $this->tiket_model->ambil_tiket(
			"selesai",
			"",
			$config['per_page'], 
			$page
		);
		$this->data['pagination'] = $this->pagination->create_links();

		// tentukan header berdasarkan level
		if (!$this->ion_auth->logged_in()) {
			$this->load->view('template/header', $this->data);
		}
		elseif ($this->ion_auth->is_admin()) {
			// header admin
			$this->load->view('template/auth_admin_header', $this->data);
		}
		else {
			$this->load->view('template/auth_header', $this->data);
		}
		$this->load->view('ticket/selesai');
		$this->load->view('template/auth_footer');
	}

	/**
	*	Detail Tiket
	*	Menampilkan detail tiket
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.1.0 (2017-02-23)
	*	
	*	@param 			string		$status
	*	@param 			string		$tiket_id
	*	@return 		void
	*	
	*/
	public function detail($status, $tiket_id)
	{
		// if (!$this->ion_auth->logged_in()) {
		// 	redirect('auth/login', 'refresh');
		// }
		// else {
			$this->data['detail_tiket'] = $this->tiket_model->detail_tiket($tiket_id)->row();
			$this->data['halaman']      = $status;
			$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			// tentukan header berdasarkan level
			if (!$this->ion_auth->logged_in()) {
				$this->load->view('template/header', $this->data);
			}
			elseif ($this->ion_auth->is_admin()) {
				// header admin
				$this->load->view('template/auth_admin_header', $this->data);
			}
			else {
				$this->load->view('template/auth_header', $this->data);
			}
			$this->load->view('ticket/detail_tiket');
			$this->load->view('template/auth_footer');
		// }
	}

	/**
	*	Ubah Tiket
	*	Menampilkan detail tiket dan form untuk melakukan
	*	perubahan data tiket atau melakukan penyimpanan
	*	data tiket yang telah dimasukkan.
	*	Hanya user group admin yang dapat melakukan akses
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.1.0 (2017-02-27)
	*	
	*	@param 			string		$status
	*	@param 			string		$tiket_id
	*	@return 		void
	*	
	*/
	public function ubah($status, $tiket_id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// validate form input
			$this->form_validation->set_rules('tanggal', 'Tanggal Tiket', 'required');
			$this->form_validation->set_rules('jenis_tiket', 'Jenis Tiket', 'required');
			$this->form_validation->set_rules('isi', 'Isi Tiket', 'required|addslashes|trim');
			$this->form_validation->set_rules('lokasi_kerja_id', 'Lokasi Kerja', 'required');
			$this->form_validation->set_rules('gedung_id', 'Gedung', 'required');
			$this->form_validation->set_rules('ruang_id', 'Ruang', 'required');
			$this->form_validation->set_rules('lantai', 'Lantai', 'required');
			$this->form_validation->set_rules('status', 'Status', 'required|addslashes|trim');
			$this->form_validation->set_rules('selesai_oleh', 'Penanganan Oleh', 'required|addslashes|trim');
			$this->form_validation->set_rules('selesai_tanggal', 'Penanganan Tanggal', 'required');
			$this->form_validation->set_rules('penanganan', 'Penanganan Tiket', 'required|addslashes|trim');

			// jika validasi berhasil jalankan proses simpan tiket
			if ($this->form_validation->run() == true) {
				$datas = array(
					'jenis_tiket_id'  => $this->input->post('jenis_tiket'),
					'tanggal'         => strtotime($this->input->post('tanggal')),
					'isi'             => $this->input->post('isi'),
					'status'          => $this->input->post('status'),
					'selesai_oleh'    => $this->input->post('selesai_oleh'),
					'selesai_tanggal' => strtotime($this->input->post('selesai_tanggal')),
					'penanganan'      => $this->input->post('penanganan'),
					'updated_by'      => $this->session->userdata('identity'),
					'updated_on'      => time(),
				);
				if ($this->tiket_model->ubah($datas, $tiket_id)) {
					// update detail
					$datas = array(
						'jabatan_id'        => $this->input->post('jabatan_id'),
						'status_pegawai_id' => $this->input->post('status_pegawai_id'),
						'unit_kerja_id'     => $this->input->post('unit_kerja_id'),
						'lokasi_kerja_id'   => $this->input->post('lokasi_kerja_id'),
						'gedung_id'         => $this->input->post('gedung_id'),
						'ruang_id'          => $this->input->post('ruang_id'),
						'lantai'            => $this->input->post('lantai'),
						'updated_by'        => $this->session->userdata('identity'),
						'updated_on'        => time(),
					);

					if ($this->tiket_model->ubah_detail($datas, $tiket_id)) {
						$this->session->set_flashdata('message', $this->message_start_delimiter.'Tiket berhasil diubah!'.$this->message_end_delimiter);
					}
					else {
						$this->session->set_flashdata('message', $this->message_start_delimiter.'Detail Tiket gagal diubah!'.$this->message_end_delimiter);
					}
				}
				else {
					$this->session->set_flashdata('message', $this->message_start_delimiter.'Tiket gagal diubah!'.$this->message_end_delimiter);
				}
				redirect('tiket/'.$status, 'refresh');
			}
			else {
				$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$detail_tiket               = $this->tiket_model->detail_tiket($tiket_id)->row();
				$this->data['tanggal_val']  = ($this->form_validation->set_value('tanggal')!="") ? $this->form_validation->set_value('tanggal') : date('Y-m-d H:i', $detail_tiket->tanggal);
				$this->data['detail_tiket'] = $detail_tiket;
				$this->data['tiket_id']     = $tiket_id;
				$this->data['halaman']      = $status;
				$this->data['kode']         = $detail_tiket->kode;
				$this->data['jenis_tiket']  = $this->tiket_model->jenis_tiket();
				$this->data['jenis_tiket_val'] = ($this->input->post('jenis_tiket')!="") ? $this->input->post('jenis_tiket') : $detail_tiket->jenis_tiket_id;
				$this->data['isi_val'] = ($this->form_validation->set_value('isi')!="") ? $this->form_validation->set_value('isi') : $detail_tiket->isi;
				$this->data['selesai_oleh'] = $this->ion_auth->users();
				$this->data['selesai_oleh_val'] = ($this->form_validation->set_value('selesai_oleh')!="") ? $this->form_validation->set_value('selesai_oleh') : $detail_tiket->selesai_oleh;
				$selesai_tanggal_val = ($detail_tiket->selesai_tanggal != '0') ? $detail_tiket->selesai_tanggal : time();
				$this->data['selesai_tanggal_val'] = ($this->form_validation->set_value('selesai_tanggal')!="") ? $this->form_validation->set_value('selesai_tanggal') : date('Y-m-d H:i', $selesai_tanggal_val);
				$this->data['penanganan_val'] = ($this->form_validation->set_value('penanganan')!="") ? $this->form_validation->set_value('penanganan') : $detail_tiket->penanganan;

				// data select untuk data yang dapat diubah
				$this->data['lokasi_kerja_data'] = $this->pengguna_tiket_model->lokasi_kerja();
				$this->data['gedung_data']       = $this->pengguna_tiket_model->gedung();
				$this->data['ruang_data']        = $this->pengguna_tiket_model->ruang();

				// validate form input
				$this->form_validation->set_rules('jenis_tiket', 'Jenis Tiket', 'required');
				$this->form_validation->set_rules('isi', 'Isi Tiket', 'required|addslashes|trim');

				// tentukan header berdasarkan level
				if (!$this->ion_auth->is_admin()) {
					$this->load->view('template/auth_header', $this->data);
				}
				// header admin
				else {
					$this->load->view('template/auth_admin_header', $this->data);
				}	
				$this->load->view('ticket/ubah');
				$this->load->view('template/auth_footer');
			}
		}
		else {
			redirect('main', 'refresh');
		}
	}

	/**
	*	Hapus Tiket
	*	Melakukan penghapusan data tiket
	*	Hanya user group admin yang dapat melakukan akses
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-27)
	*	
	*	@param 			string		$status
	*	@param 			string		$tiket_id
	*	@return 		void
	*	
	*/
	public function hapus($status, $tiket_id)
	{
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// validate form input
			$this->form_validation->set_rules('id', 'ID Tiket', 'required');
			// jika validasi berhasil jalankan proses simpan tiket
			if ($this->form_validation->run() == true) {
				$datas = array(
					'hapus'      => '1',
					'updated_by' => $this->session->userdata('identity'),
					'updated_on' => time(),
				);
				if ($this->tiket_model->hapus($datas, $tiket_id)) {
					$this->session->set_flashdata('message', $this->message_start_delimiter.'Tiket berhasil dihapus!'.$this->message_end_delimiter);
				}
				else {
					$this->session->set_flashdata('message', $this->message_start_delimiter.'Detail Tiket gagal dihapus!'.$this->message_end_delimiter);
				}
				redirect('tiket/'.$status, 'refresh');
			}
			else {
				$this->data['detail_tiket'] = $this->tiket_model->detail_tiket($tiket_id)->row();
				$this->data['tiket_id']     = $tiket_id;
				$this->data['halaman']      = $status;
				$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				// tentukan header berdasarkan level
				if (!$this->ion_auth->is_admin()) {
					$this->load->view('template/auth_header', $this->data);
				}
				// header admin
				else {
					$this->load->view('template/auth_admin_header', $this->data);
				}	
				$this->load->view('ticket/hapus');
				$this->load->view('template/auth_footer');
			}
		}
		else {
			redirect('main', 'refresh');
		}
	}



}
/* End of Tiket.php */