<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
*	Jabatan Controller
*	
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*	
*	Melakukan pengaturan dasar data jabatan
*	
*/
class Jabatan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set delimeters
		$this->message_start_delimiter = $this->config->item('message_start_delimiter', 'ion_auth');
		$this->message_end_delimiter   = $this->config->item('message_end_delimiter', 'ion_auth');
		$this->error_start_delimiter   = $this->config->item('error_start_delimiter', 'ion_auth');
		$this->error_end_delimiter     = $this->config->item('error_end_delimiter', 'ion_auth');

		// set error delimeters
		$this->form_validation->set_error_delimiters(
			$this->error_start_delimiter, 
			$this->error_end_delimiter
		);

		// load site config
		$this->load->config('site', TRUE);

		// load model
		$this->load->model(array('jabatan_model'));

		// set default setting based on site config
		$this->data['site_title']  = $this->config->item('site_title', 'site');
		$this->data['site_slogan'] = $this->config->item('site_slogan', 'site');
		$this->data['page']        = "master";
	}

	// redirect if needed, otherwise display the jabatan list
	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// set the flash data error message if there is one
			$this->data['message']     = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['detail']      = $this->jabatan_model->daftar();
			$this->data['master_page'] = "jabatan";

			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('master/header');
			$this->load->view('master/jabatan');
			$this->load->view('master/footer');
			$this->load->view('template/auth_footer');
		}
		else {
			redirect('main', 'refresh');
		}
	}

	// Tambah jabatan
	public function tambah()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// validasi (jika ada)
			$this->form_validation->set_rules('nama', 'Nama', 'required|addslashes|trim|alpha_numeric_spaces');

			if ($this->form_validation->run() == true) {
				$datas['nama']  = $this->input->post('nama');
				$datas['aktif'] = $this->input->post('aktif');
				$this->data['master_page'] = "jabatan";

				if ($this->jabatan_model->simpan($datas)) {
					$this->session->set_flashdata('message', 
						$this->message_start_delimiter . 'Data berhasil disimpan!' . $this->message_end_delimiter);
				} else {
					$this->session->set_flashdata('message', 
						$this->error_start_delimiter . 'Data gagal disimpan!' . $this->error_end_delimiter);
				}

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				redirect('jabatan', 'refresh');
			}
			else {
				// set the flash data error message if there is one
				$this->data['message']     = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->data['master_page'] = "jabatan";

				$this->load->view('template/auth_admin_header', $this->data);
				$this->load->view('master/header');
				$this->load->view('master/jabatan_tambah');
				$this->load->view('master/footer');
				$this->load->view('template/auth_footer');
			}
		}
		else {
			redirect('main', 'refresh');
		}
	}

	// Ubah jabatan
	public function ubah($id='')
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// validasi (jika ada)
			$this->form_validation->set_rules('nama', 'Nama', 'required|addslashes|trim|alpha_numeric_spaces');

			if ($this->form_validation->run() == true) {
				$datas['id']    = $this->input->post('id');
				$datas['nama']  = $this->input->post('nama');
				$datas['aktif'] = $this->input->post('aktif');

				if ($this->jabatan_model->ubah($datas)) {
					$this->session->set_flashdata('message', 
						$this->message_start_delimiter . 'Data berhasil diubah!' . $this->message_end_delimiter);
				} else {
					$this->session->set_flashdata('message', 
						$this->error_start_delimiter . 'Data gagal diubah!' . $this->error_end_delimiter);
				}

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				redirect('jabatan', 'refresh');
			}
			else {
				// set the flash data error message if there is one
				$this->data['message']     = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->data['detail']      = $this->jabatan_model->daftar($id);
				$this->data['master_page'] = "jabatan";
				
				$this->load->view('template/auth_admin_header', $this->data);
				$this->load->view('master/header');
				$this->load->view('master/jabatan_ubah');
				$this->load->view('master/footer');
				$this->load->view('template/auth_footer');
			}
		}
		else {
			redirect('main', 'refresh');
		}
	}

	// Hapus jabatan
	public function hapus($id='')
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			if ($id!="") {
				if ($this->jabatan_model->hapus($id)) {
					$this->session->set_flashdata('message', 
						$this->message_start_delimiter . 'Data berhasil dihapus!' . $this->message_end_delimiter);
				} else {
					$this->session->set_flashdata('message', 
						$this->error_start_delimiter . 'Data gagal dihapus!' . $this->error_end_delimiter);
				}
			}
			else {
				$this->session->set_flashdata('message', 
					$this->error_start_delimiter . 'Data gagal dihapus!' . $this->error_end_delimiter);
			}

			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			redirect('jabatan', 'refresh');
		}
		else {
			redirect('main', 'refresh');
		}
	}

}