<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
*	Laporan Controller
*	
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*	
*	Controller untuk melakukan pembuatan laporan tiket
*	
*/
class Laporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set error delimeters
		$this->form_validation->set_error_delimiters(
			$this->config->item('error_start_delimiter', 'ion_auth'), 
			$this->config->item('error_end_delimiter', 'ion_auth')
		);

		// set delimeters
		$this->message_start_delimiter = $this->config->item('message_start_delimiter', 'ion_auth');
		$this->message_end_delimiter   = $this->config->item('message_end_delimiter', 'ion_auth');
		$this->error_start_delimiter   = $this->config->item('error_start_delimiter', 'ion_auth');
		$this->error_end_delimiter     = $this->config->item('error_end_delimiter', 'ion_auth');

		// load site config
		$this->load->config('site', TRUE);

		// set default setting based on site config
		$this->data['site_title']  = $this->config->item('site_title', 'site');
		$this->data['site_slogan'] = $this->config->item('site_slogan', 'site');
		$this->data['page']        = "laporan";

		// load model
		$this->load->model(array(
			'laporan_model', 
			'tiket_model', 
			'pengguna_tiket_model',
			'jabatan_model',
			'unit_kerja_model',
			'status_pegawai_model',
			'lokasi_kerja_model',
			'gedung_model',
			'ruang_model',
		));

		// pagination
		$this->load->library(array('pagination'));
		$this->load->config('pagination');
	}

	/**
	*	Index
	*	Menampilkan halaman utama laporan
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-03-03)
	*	
	*	@return 		void
	*	
	*/
	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// set the flash data error message if there is one
			$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['laporan_page'] = "index";

			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('report/header');
			$this->load->view('report/index');
			$this->load->view('report/footer');
			$this->load->view('template/auth_footer');
		}
		else {
			redirect('main', 'refresh');
		}
	}

	/**
	*	Laporan Tiket
	*	Menampilkan halaman utama yang berisi form untuk melakukan
	*	pembuatan laporan tiket yang dapat disesuaikan dengan
	*	kriteria tertentu.
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-03-03)
	*	
	*	@return 		void
	*	
	*/
	public function tiket()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['laporan_page'] = "tiket";
			$this->data['jenis_tiket']  = $this->tiket_model->jenis_tiket();
			$this->data['oleh']         = $this->ion_auth->users();
			$this->data['selesai_oleh'] = $this->ion_auth->users();
			$this->data['unit_kerja']   = $this->unit_kerja_model->daftar();
			$this->data['lokasi_kerja'] = $this->lokasi_kerja_model->daftar();
			$this->data['gedung']       = $this->gedung_model->daftar();
			$this->data['ruang']        = $this->ruang_model->daftar();

			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('report/header');
			$this->load->view('report/laporan_tiket');
			$this->load->view('report/footer');
			$this->load->view('template/auth_footer');
		}
		else {
			redirect('main', 'refresh');
		}
	}

	/**
	*	Laporan Tiket
	*	Menerima semua hasil inputan dari halaman laporan tiket
	*	dan melakukan pembuatan laporan berdasarkan jenis laporan
	*	yang dipilih (PDF / Excel)
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-03-03)
	*	
	*	@return 		void
	*	
	*/
	public function tiket_hasil()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['laporan_page'] = "tiket";

			// ambil semua data kiriman post
			$this->data['jenis_tiket']   = $this->input->post('jenis_tiket');
			$this->data['oleh']          = $this->input->post('oleh');
			$this->data['selesai_oleh']  = $this->input->post('selesai_oleh');
			$this->data['status']        = $this->input->post('status');
			$this->data['unit_kerja']    = $this->input->post('unit_kerja');
			$this->data['lokasi_kerja']  = $this->input->post('lokasi_kerja');
			$this->data['gedung']        = $this->input->post('gedung');
			$this->data['ruang']         = $this->input->post('ruang');
			$this->data['lantai']        = $this->input->post('lantai');
			$this->data['tanggal_awal']  = "";
			$this->data['tanggal_akhir'] = "";
			if ($this->input->post('tanggal_awal')!="" && $this->input->post('tanggal_akhir')!="") {
				$this->data['tanggal_awal']  = strtotime($this->input->post('tanggal_awal'). " 00:00:00");
				$this->data['tanggal_akhir'] = strtotime($this->input->post('tanggal_akhir'). " 23:59:59");
			}
			$this->data['selesai_tanggal_awal']  = "";
			$this->data['selesai_tanggal_akhir'] = "";
			if ($this->input->post('selesai_tanggal_awal')!="" && $this->input->post('selesai_tanggal_akhir')!="") {
				$this->data['selesai_tanggal_awal']  = strtotime($this->input->post('selesai_tanggal_awal'). " 00:00:00");
				$this->data['selesai_tanggal_akhir'] = strtotime($this->input->post('selesai_tanggal_akhir'). " 23:59:59");
			}

			$this->data['isi_laporan'] = $this->laporan_model->laporan_tiket($this->data);

			// ambil jenis laporan
			if ($this->input->post('laporan_pdf')) {
				$this->load->view('report/laporan_tiket_pdf', $this->data);
			}
			elseif ($this->input->post('laporan_excel')) {
				$this->load->view('report/laporan_tiket_excel', $this->data);
			}
		}
		else {
			redirect('main', 'refresh');
		}
	}

	/**
	*	Laporan Tiket per user
	*	Menampilkan halaman utama yang berisi form untuk melakukan
	*	pembuatan laporan tiket yang dapat disesuaikan dengan
	*	kriteria tertentu.
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-03-06)
	*	
	*	@return 		void
	*	
	*/
	public function tiket_per_user()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['laporan_page'] = "tiket_per_user";
			$this->data['jenis_tiket']  = $this->tiket_model->jenis_tiket();
			$this->data['oleh']         = $this->ion_auth->users();
			$this->data['selesai_oleh'] = $this->ion_auth->users();
			$this->data['unit_kerja']   = $this->unit_kerja_model->daftar();

			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('report/header');
			$this->load->view('report/laporan_tiket_per_user');
			$this->load->view('report/footer');
			$this->load->view('template/auth_footer');
		}
		else {
			redirect('main', 'refresh');
		}
	}

	/**
	*	Laporan Tiket Per User
	*	Menerima semua hasil inputan dari halaman laporan tiket
	*	dan melakukan pembuatan laporan berdasarkan jenis laporan
	*	yang dipilih (PDF / Excel)
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-03-06)
	*	
	*	@return 		void
	*	
	*/
	public function tiket_per_user_hasil()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			$this->data['message']      = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['laporan_page'] = "tiket";

			// ambil semua data kiriman post
			$this->data['jenis_tiket']   = $this->input->post('jenis_tiket');
			$this->data['oleh']          = $this->input->post('oleh');
			$this->data['status']        = $this->input->post('status');
			$this->data['unit_kerja']    = $this->input->post('unit_kerja');
			$this->data['tanggal_awal']  = "";
			$this->data['tanggal_akhir'] = "";
			if ($this->input->post('tanggal_awal')!="" && $this->input->post('tanggal_akhir')!="") {
				$this->data['tanggal_awal']  = strtotime($this->input->post('tanggal_awal'). " 00:00:00");
				$this->data['tanggal_akhir'] = strtotime($this->input->post('tanggal_akhir'). " 23:59:59");
			}
			$this->data['selesai_tanggal_awal']  = "";
			$this->data['selesai_tanggal_akhir'] = "";
			if ($this->input->post('selesai_tanggal_awal')!="" && $this->input->post('selesai_tanggal_akhir')!="") {
				$this->data['selesai_tanggal_awal']  = strtotime($this->input->post('selesai_tanggal_awal'). " 00:00:00");
				$this->data['selesai_tanggal_akhir'] = strtotime($this->input->post('selesai_tanggal_akhir'). " 23:59:59");
			}

			$this->data['isi_laporan'] = $this->laporan_model->laporan_tiket_per_user($this->data);

			// ambil jenis laporan
			if ($this->input->post('laporan_pdf')) {
				$this->load->view('report/laporan_tiket_per_user_pdf', $this->data);
			}
			elseif ($this->input->post('laporan_excel')) {
				$this->load->view('report/laporan_tiket_per_user_excel', $this->data);
			}
		}
		else {
			redirect('main', 'refresh');
		}
	}

}

/* End of laporan.php */