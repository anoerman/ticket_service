<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
*	Pengguna Tiket Controller
*	
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*	
*	Controller semua kegiatan yang terjadi pada halaman pengguna tiket
*	Hanya dapat diakses oleh user dengan group admin
*	Melakukan berbagai pengaturan yang berkaitan dengan pengguna tiket
*	
*	
*/
class Pengguna_tiket extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set error delimeters
		$this->form_validation->set_error_delimiters(
			$this->config->item('error_start_delimiter', 'ion_auth'), 
			$this->config->item('error_end_delimiter', 'ion_auth')
		);

		// load site config
		$this->load->config('site', TRUE);

		// set default setting based on site config
		$this->data['site_title']  = $this->config->item('site_title', 'site');
		$this->data['site_slogan'] = $this->config->item('site_slogan', 'site');
		$this->data['page']        = "pengguna_tiket";

		// load model
		$this->load->model(array('pengguna_tiket_model'));
	}

	// redirect if needed, otherwise display the user list
	/**
	*	Index
	*	Menampilkan daftar pengguna tiket
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@return 		void
	*	
	*/
	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		// remove this elseif if you want to enable this for non-admins
		elseif (!$this->ion_auth->is_admin()) {
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else {
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('users/pengguna_tiket');
			$this->load->view('template/auth_footer');
		}
	}


	/**
	*	Tambah
	*	Menampilkan formulir tambah pengguna baru atau
	*	menyimpan data pengguna setelah formulir diisi
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@return 		void
	*	
	*/
	public function tambah()
	{
		// if not logged in or not admin
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
			redirect('auth', 'refresh');
		}

		$tables                        = $this->config->item('tables','ion_auth');
		$identity_column               = $this->config->item('identity','ion_auth');
		$this->data['identity_column'] = $identity_column;

		// validate form input
		$this->form_validation->set_rules('first_name', 
			$this->lang->line('create_user_validation_fname_label'), 
			'required|addslashes|trim');
		$this->form_validation->set_rules('last_name', 
			$this->lang->line('create_user_validation_lname_label'), 
			'addslashes|trim');

		if($identity_column!=='email') {
			$this->form_validation->set_rules('identity',
				$this->lang->line('create_user_validation_identity_label'),
				'required|is_unique['.$tables['users'].'.'.$identity_column.']');
			$this->form_validation->set_rules('email', 
				$this->lang->line('create_user_validation_email_label'), 
				'valid_email');
		}
		else {
			$this->form_validation->set_rules('email', 
				$this->lang->line('create_user_validation_email_label'), 
				'required|valid_email|is_unique[' . $tables['users'] . '.email]');
		}

		$this->form_validation->set_rules('password', 
			$this->lang->line('create_user_validation_password_label'), 
			'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', 
			$this->lang->line('create_user_validation_password_confirm_label'), 
			'required');
		$this->form_validation->set_rules('lantai', 
			"Lantai", 
			'required|numeric|addslashes|trim');

		// jika validasi berhasil jalankan proses register
		if ($this->form_validation->run() == true) {
			$email    = strtolower($this->input->post('email'));
			$identity = ($identity_column==='email') ? $email : $this->input->post('identity');
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
			);

			// proses register
			$id = $this->ion_auth->register($identity, $password, $email, $additional_data);
			$this->session->set_flashdata('message', $this->ion_auth->messages());

			// proses simpan user detail
			$this->pengguna_tiket_model->simpan_users_jabatan($id, $this->input->post('jabatan'));
			$this->pengguna_tiket_model->simpan_users_status_pegawai($id, $this->input->post('status_pegawai'));
			$this->pengguna_tiket_model->simpan_users_unit_kerja($id, $this->input->post('unit_kerja'));
			$this->pengguna_tiket_model->simpan_users_lokasi_kerja($id, $this->input->post('lokasi_kerja'));
			$this->pengguna_tiket_model->simpan_users_gedung($id, $this->input->post('gedung'));
			$this->pengguna_tiket_model->simpan_users_ruang($id, $this->input->post('ruang'));
			$this->pengguna_tiket_model->simpan_users_lantai($id, $this->input->post('lantai'));

			redirect("pengguna_tiket", 'refresh');
		}
		else {
			// display the create user form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('first_name'),
				'class' => 'form-control',
				'placeholder' => 'Nama Awal',
			);
			$this->data['last_name'] = array(
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('last_name'),
				'class' => 'form-control',
				'placeholder' => 'Nama Akhir',
			);
			$this->data['identity'] = array(
				'name'  => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
				'class' => 'form-control',
				'placeholder' => 'NIP',
			);
			$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email'),
				'class' => 'form-control',
				'placeholder' => 'Email',
			);
			$this->data['password'] = array(
				'name'  => 'password',
				'id'    => 'password',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password'),
				'class' => 'form-control',
				'placeholder' => 'Password',
			);
			$this->data['password_confirm'] = array(
				'name'  => 'password_confirm',
				'id'    => 'password_confirm',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
				'class' => 'form-control',
				'placeholder' => 'Konfirmasi Password',
			);
			
			// detail tambahan
			$this->data['jabatan_data']        = $this->pengguna_tiket_model->jabatan();
			$this->data['status_pegawai_data'] = $this->pengguna_tiket_model->status_pegawai();
			$this->data['unit_kerja_data']     = $this->pengguna_tiket_model->unit_kerja();
			$this->data['lokasi_kerja_data']   = $this->pengguna_tiket_model->lokasi_kerja();
			$this->data['gedung_data']         = $this->pengguna_tiket_model->gedung();
			$this->data['ruang_data']          = $this->pengguna_tiket_model->ruang();

			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('users/pengguna_tambah');
			$this->load->view('template/auth_footer');
		}
	}


	// edit user
	/**
	*	Ubah
	*	Menampilkan formulir untuk mengubah data pengguna 
	*	atau melakukan update data setelah proses perubahan
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 			string 			$id
	*	@return 		void
	*	
	*/
	public function ubah($id)
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
			redirect('auth', 'refresh');
		}

		$user   = $this->ion_auth->user($id)->row();
		$groups = $this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', 
			$this->lang->line('edit_user_validation_fname_label'), 
			'required|addslashes|trim');
		$this->form_validation->set_rules('last_name', 
			$this->lang->line('edit_user_validation_lname_label'), 
			'addslashes|trim');
		$this->form_validation->set_rules('email', 
			$this->lang->line('edit_email_validation_lname_label'), 
			'addslashes|trim|valid_email');
		$this->form_validation->set_rules('lantai', 
			"Lantai", 
			'required|numeric|addslashes|trim');

		if (isset($_POST) && !empty($_POST)) {
			// update the password if it was posted
			if ($this->input->post('password')) {
				$this->form_validation->set_rules('password', 
					$this->lang->line('edit_user_validation_password_label'), 
					'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', 
					$this->lang->line('edit_user_validation_password_confirm_label'), 
					'required');
			}

			if ($this->form_validation->run() === TRUE) {
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
				);

				// update the password if it was posted
				if ($this->input->post('password')) {
					$data['password'] = $this->input->post('password');
				}

				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin()) {
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}
					}
				}

				// check to see if we are updating the user
				if($this->ion_auth->update($user->id, $data)) {
					// update detail
					$this->pengguna_tiket_model->ubah_users_jabatan($id, $this->input->post('jabatan'));
					$this->pengguna_tiket_model->ubah_users_status_pegawai($id, $this->input->post('status_pegawai'));
					$this->pengguna_tiket_model->ubah_users_unit_kerja($id, $this->input->post('unit_kerja'));
					$this->pengguna_tiket_model->ubah_users_lokasi_kerja($id, $this->input->post('lokasi_kerja'));
					$this->pengguna_tiket_model->ubah_users_gedung($id, $this->input->post('gedung'));
					$this->pengguna_tiket_model->ubah_users_ruang($id, $this->input->post('ruang'));
					$this->pengguna_tiket_model->ubah_users_lantai($id, $this->input->post('lantai'));

					// redirect them back to the admin page if admin, or to the base url if non admin
					$this->session->set_flashdata('message', $this->ion_auth->messages() );

					redirect('pengguna_tiket', 'refresh');
				}
				else {
					// redirect them back to the admin page if admin, or to the base url if non admin
					$this->session->set_flashdata('message', $this->ion_auth->errors() );
					
					redirect('pengguna_tiket', 'refresh');
				}
			}
		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['username'] = array(
			'name'  => 'username',
			'id'    => 'username',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('username', $user->username),
			'class' => 'form-control',
			'disabled' => '',
		);
		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
			'class' => 'form-control',
			'placeholder' => 'Nama Awal',
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
			'class' => 'form-control',
			'placeholder' => 'Nama Akhir',
		);
		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('email', $user->email),
			'class' => 'form-control',
			'placeholder' => 'Email',
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password',
			'class' => 'form-control',
			'placeholder' => 'Password',
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password',
			'class' => 'form-control',
			'placeholder' => 'Konfirmasi Password',
		);

		// detail tambahan
		$this->data['jabatan_data']        = $this->pengguna_tiket_model->jabatan();
		$this->data['status_pegawai_data'] = $this->pengguna_tiket_model->status_pegawai();
		$this->data['unit_kerja_data']     = $this->pengguna_tiket_model->unit_kerja();
		$this->data['lokasi_kerja_data']   = $this->pengguna_tiket_model->lokasi_kerja();
		$this->data['gedung_data']         = $this->pengguna_tiket_model->gedung();
		$this->data['ruang_data']          = $this->pengguna_tiket_model->ruang();
		$this->data['jabatan_sekarang']        = $this->pengguna_tiket_model->ambil_users_jabatan($id)->row();
		$this->data['status_pegawai_sekarang'] = $this->pengguna_tiket_model->ambil_users_status_pegawai($id)->row();
		$this->data['unit_kerja_sekarang']     = $this->pengguna_tiket_model->ambil_users_unit_kerja($id)->row();
		$this->data['lokasi_kerja_sekarang']   = $this->pengguna_tiket_model->ambil_users_lokasi_kerja($id)->row();
		$this->data['gedung_sekarang']         = $this->pengguna_tiket_model->ambil_users_gedung($id)->row();
		$this->data['ruang_sekarang']          = $this->pengguna_tiket_model->ambil_users_ruang($id)->row();
		$this->data['lantai_sekarang']         = $this->pengguna_tiket_model->ambil_users_lantai($id)->row();

		$this->load->view('template/auth_admin_header', $this->data);
		$this->load->view('users/pengguna_ubah');
		$this->load->view('template/auth_footer');
	}


	/**
	*	Detail
	*	Menampilkan detail pengguna tiket terpilih
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 			string 			$id
	*	@return 		void
	*	
	*/
	public function detail($id)
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		// remove this elseif if you want to enable this for non-admins
		elseif (!$this->ion_auth->is_admin()) {
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else {
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->user($id)->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->data['jabatan_sekarang']        = $this->pengguna_tiket_model->ambil_users_jabatan($id)->row();
			$this->data['status_pegawai_sekarang'] = $this->pengguna_tiket_model->ambil_users_status_pegawai($id)->row();
			$this->data['unit_kerja_sekarang']     = $this->pengguna_tiket_model->ambil_users_unit_kerja($id)->row();
			$this->data['lokasi_kerja_sekarang']   = $this->pengguna_tiket_model->ambil_users_lokasi_kerja($id)->row();
			$this->data['gedung_sekarang']         = $this->pengguna_tiket_model->ambil_users_gedung($id)->row();
			$this->data['ruang_sekarang']          = $this->pengguna_tiket_model->ambil_users_ruang($id)->row();
			$this->data['lantai_sekarang']         = $this->pengguna_tiket_model->ambil_users_lantai($id)->row();

			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('users/pengguna_detail');
			$this->load->view('template/auth_footer');
		}
	}


	/**
	*	Activate
	*	Melakukan aktifasi pengguna yang sebelumnya inaktif
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 			string 			$id
	*	@param 			string 			$code
	*	@return 		void
	*	
	*/
	public function activate($id, $code=false)
	{
		if ($code !== false) {
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin()) {
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation) {
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("pengguna_tiket", 'refresh');
		}
		else {
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}


	/**
	*	Deactivate
	*	Me non aktifkan  pengguna yang sebelumnya aktif
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 			string 			$id
	*	@return 		void
	*	
	*/
	public function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();
			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('users/deactivate_user');
			$this->load->view('template/auth_footer');
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			// redirect them back to the auth page
			redirect('pengguna_tiket', 'refresh');
		}
	}


	// CSRF
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	public function _valid_csrf_nonce()
	{
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

}
