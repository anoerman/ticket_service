<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
*	Status_pegawai Controller
*	
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*	
*	Melakukan pengaturan dasar data Status_pegawai
*	
*/
class Status_pegawai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set delimeters
		$this->message_start_delimiter = $this->config->item('message_start_delimiter', 'ion_auth');
		$this->message_end_delimiter   = $this->config->item('message_end_delimiter', 'ion_auth');
		$this->error_start_delimiter   = $this->config->item('error_start_delimiter', 'ion_auth');
		$this->error_end_delimiter     = $this->config->item('error_end_delimiter', 'ion_auth');

		// set error delimeters
		$this->form_validation->set_error_delimiters(
			$this->error_start_delimiter, 
			$this->error_end_delimiter
		);

		// load site config
		$this->load->config('site', TRUE);

		// load model
		$this->load->model(array('status_pegawai_model'));

		// set default setting based on site config
		$this->data['site_title']  = $this->config->item('site_title', 'site');
		$this->data['site_slogan'] = $this->config->item('site_slogan', 'site');
		$this->data['page']        = "master";
	}

	// redirect if needed, otherwise display the status_pegawai list
	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// set the flash data error message if there is one
			$this->data['message']     = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['detail']      = $this->status_pegawai_model->daftar();
			$this->data['master_page'] = "status_pegawai";

			$this->load->view('template/auth_admin_header', $this->data);
			$this->load->view('master/header');
			$this->load->view('master/status_pegawai');
			$this->load->view('master/footer');
			$this->load->view('template/auth_footer');
		}
		else {
			redirect('main', 'refresh');
		}
	}

	// Tambah status_pegawai
	public function tambah()
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// validasi (jika ada)
			$this->form_validation->set_rules('nama', 'Nama', 'required|addslashes|trim|alpha_numeric_spaces');

			if ($this->form_validation->run() == true) {
				$datas['nama']  = $this->input->post('nama');
				$datas['aktif'] = $this->input->post('aktif');

				if ($this->status_pegawai_model->simpan($datas)) {
					$this->session->set_flashdata('message', 
						$this->message_start_delimiter . 'Data berhasil disimpan!' . $this->message_end_delimiter);
				} else {
					$this->session->set_flashdata('message', 
						$this->error_start_delimiter . 'Data gagal disimpan!' . $this->error_end_delimiter);
				}

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				redirect('status_pegawai', 'refresh');
			}
			else {
				// set the flash data error message if there is one
				$this->data['message']     = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->data['master_page'] = "status_pegawai";

				$this->load->view('template/auth_admin_header', $this->data);
				$this->load->view('master/header');
				$this->load->view('master/status_pegawai_tambah');
				$this->load->view('master/footer');
				$this->load->view('template/auth_footer');
			}
		}
		else {
			redirect('main', 'refresh');
		}
	}

	// Ubah status_pegawai
	public function ubah($id='')
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			// validasi (jika ada)
			$this->form_validation->set_rules('nama', 'Nama', 'required|addslashes|trim|alpha_numeric_spaces');

			if ($this->form_validation->run() == true) {
				$datas['id']    = $this->input->post('id');
				$datas['nama']  = $this->input->post('nama');
				$datas['aktif'] = $this->input->post('aktif');

				if ($this->status_pegawai_model->ubah($datas)) {
					$this->session->set_flashdata('message', 
						$this->message_start_delimiter . 'Data berhasil diubah!' . $this->message_end_delimiter);
				} else {
					$this->session->set_flashdata('message', 
						$this->error_start_delimiter . 'Data gagal diubah!' . $this->error_end_delimiter);
				}

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				redirect('status_pegawai', 'refresh');
			}
			else {
				// set the flash data error message if there is one
				$this->data['message']     = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->data['detail']      = $this->status_pegawai_model->daftar($id);
				$this->data['master_page'] = "status_pegawai";

				$this->load->view('template/auth_admin_header', $this->data);
				$this->load->view('master/header');
				$this->load->view('master/status_pegawai_ubah');
				$this->load->view('master/footer');
				$this->load->view('template/auth_footer');
			}
		}
		else {
			redirect('main', 'refresh');
		}
	}

	// Hapus status_pegawai
	public function hapus($id='')
	{
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif ($this->ion_auth->is_admin()) {
			if ($id!="") {
				if ($this->status_pegawai_model->hapus($id)) {
					$this->session->set_flashdata('message', 
						$this->message_start_delimiter . 'Data berhasil dihapus!' . $this->message_end_delimiter);
				} else {
					$this->session->set_flashdata('message', 
						$this->error_start_delimiter . 'Data gagal dihapus!' . $this->error_end_delimiter);
				}
			}
			else {
				$this->session->set_flashdata('message', 
					$this->error_start_delimiter . 'Data gagal dihapus!' . $this->error_end_delimiter);
			}

			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			redirect('status_pegawai', 'refresh');
		}
		else {
			redirect('main', 'refresh');
		}
	}

}