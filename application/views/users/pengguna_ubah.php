<div class="container">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h2>Pengguna</h2>
    <p>Ubah data pengguna</p>
    <hr>
    <?php echo $message;?>
  </div>
  <?php echo form_open("pengguna_tiket/ubah/".$user->id, array('autocomplete' => 'off'));?>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
      <label for="Username" class="control-label">Username / NIP</label> <br />
      <?php echo form_input($username);?>
    </div>

    <div class="form-group">
      <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
      <?php //echo form_error('first_name');?>
      <?php echo form_input($first_name);?>
    </div>

    <div class="form-group">
      <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
      <?php //echo form_error('last_name');?>
      <?php echo form_input($last_name);?>
    </div>

    <div class="form-group">
      <?php echo lang('edit_user_email_label', 'email');?> <br />
      <?php //echo form_error('email');?>
      <?php echo form_input($email);?>
    </div>

    <div class="form-group">
      <?php echo lang('edit_user_password_label', 'password');?> <br />
      <?php //echo form_error('password');?>
      <?php echo form_input($password);?>
    </div>

    <div class="form-group">
      <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?> <br />
      <?php //echo form_error('password_confirm');?>
      <?php echo form_input($password_confirm);?>
    </div>

    <hr>

    <?php if ($this->ion_auth->is_admin()): ?>
    <div class="form-group">
      <h3><?php echo lang('edit_user_groups_heading');?></h3>
      <?php foreach ($groups as $group):?>
        <div class="checkbox">          
        <label>
        <?php
          $gID     = $group['id'];
          $checked = null;
          $item    = null;
          foreach($currentGroups as $grp) {
            if ($gID == $grp->id) {
              $checked= ' checked="checked"';
            break;
            }
          }
        ?>
        <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
        <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
        </label>
        </div>
      <?php endforeach?>
    </div>
    <?php endif ?>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
      <label for="jabatan" class="control-label">Jabatan</label> <br />
      <select name="jabatan" id="jabatan" class="form-control">
        <option value="">Pilih Jabatan</option>
      <?php foreach ($jabatan_data->result() as $jabatan): ?>
        <option value="<?php echo $jabatan->id ?>" <?php echo ($jabatan->id == $jabatan_sekarang->jabatan_id) ? "selected" : ""; ?>><?php echo $jabatan->nama ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-group">
      <label for="status_pegawai" class="control-label">Status Pegawai</label> <br />
      <select name="status_pegawai" id="status_pegawai" class="form-control">
        <option value="">Pilih Status Pegawai</option>
      <?php foreach ($status_pegawai_data->result() as $status_pegawai): ?>
        <option value="<?php echo $status_pegawai->id ?>" <?php echo ($status_pegawai->id == $status_pegawai_sekarang->status_pegawai_id) ? "selected" : ""; ?>><?php echo $status_pegawai->nama ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-group">
      <label for="unit_kerja" class="control-label">Unit Kerja</label> <br />
      <select name="unit_kerja" id="unit_kerja" class="form-control">
        <option value="">Pilih Unit Kerja</option>
      <?php foreach ($unit_kerja_data->result() as $unit_kerja): ?>
        <option value="<?php echo $unit_kerja->id ?>" <?php echo ($unit_kerja->id == $unit_kerja_sekarang->unit_kerja_id) ? "selected" : ""; ?>><?php echo $unit_kerja->nama ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-group">
      <label for="lokasi_kerja" class="control-label">Lokasi Kerja</label> <br />
      <select name="lokasi_kerja" id="lokasi_kerja" class="form-control">
        <option value="">Pilih Lokasi Kerja</option>
      <?php foreach ($lokasi_kerja_data->result() as $lokasi_kerja): ?>
        <option value="<?php echo $lokasi_kerja->id ?>" <?php echo ($lokasi_kerja->id == $lokasi_kerja_sekarang->lokasi_kerja_id) ? "selected" : ""; ?>><?php echo $lokasi_kerja->nama ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-group">
      <label for="gedung" class="control-label">Gedung</label> <br />
      <select name="gedung" id="gedung" class="form-control">
        <option value="">Pilih Gedung</option>
      <?php foreach ($gedung_data->result() as $gedung): ?>
        <option value="<?php echo $gedung->id ?>" <?php echo ($gedung->id == $gedung_sekarang->gedung_id) ? "selected" : ""; ?>><?php echo $gedung->nama ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-group">
      <label for="ruang" class="control-label">Ruang</label> <br />
      <select name="ruang" id="ruang" class="form-control">
        <option value="">Pilih Ruang</option>
      <?php foreach ($ruang_data->result() as $ruang): ?>
        <option value="<?php echo $ruang->id ?>" <?php echo ($ruang->id == $ruang_sekarang->ruang_id) ? "selected" : ""; ?>><?php echo $ruang->nama ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-group">
      <label for="lantai" class="control-label">Lantai</label> <br />
      <input type="number" name="lantai" id="lantai" class="form-control" placeholder="Lantai" value="<?php echo $lantai_sekarang->lantai ?>">
    </div>
    
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <hr>
    <div class="form-group text-center">
      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_submit('submit', lang('edit_user_submit_btn'), $arrayName = array('class' => 'btn btn-primary',));?>
      <?php echo anchor('pengguna_tiket', 'Kembali', $arrayName = array('class' => 'btn btn-default',));?>
    </div>
  </div>
  <?php echo form_close();?>
</div>