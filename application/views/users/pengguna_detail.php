<div class="container">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2>Detail Pengguna</h2>
		<hr>
		<?php echo $message;?>
	</div>
	
	<?php 
	// Ambil semua data dan set dalam variabel
	foreach ($users as $user) {
		$user_username   = $user->username;
		$user_first_name = $user->first_name;
		$user_last_name  = $user->last_name;
		$user_email      = $user->email;
	}
	?>

	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-group">
			<label for="username" class="control-label">Username / NIP</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $user_username;?>">
		</div>

		<div class="form-group">
			<label for="nama" class="control-label">Nama</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $user_first_name . " " . $user_last_name;?>">
		</div>

		<div class="form-group">
			<label for="email" class="control-label">Email</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $user_email;?>">
		</div>

		<div class="form-group">
			<label for="group" class="control-label">Group</label> <br />
			<?php foreach ($user->groups as $group):?>
				<?php echo "<button class='btn btn-primary btn-sm'>" . htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') . "</button>" ;?>
			<?php endforeach?>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-group">
			<label for="jabatan" class="control-label">Jabatan</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $jabatan_sekarang->nama ?>">
		</div>

		<div class="form-group">
			<label for="status_pegawai" class="control-label">Status Pegawai</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $status_pegawai_sekarang->nama ?>">
		</div>

		<div class="form-group">
			<label for="unit_kerja" class="control-label">Unit Kerja</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $unit_kerja_sekarang->nama ?>">
		</div>

		<div class="form-group">
			<label for="lokasi_kerja" class="control-label">Lokasi Kerja</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $lokasi_kerja_sekarang->nama ?>">
		</div>

		<div class="form-group">
			<label for="gedung" class="control-label">Gedung</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $gedung_sekarang->nama ?>">
		</div>

		<div class="form-group">
			<label for="ruang" class="control-label">Ruang</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $ruang_sekarang->nama ?>">
		</div>

		<div class="form-group">
			<label for="lantai" class="control-label">Lantai</label> <br />
			<input type="text" class="form-control" disabled="" value="<?php echo $lantai_sekarang->lantai ?>">
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<hr>
		<div class="form-group text-center">
			<?php echo anchor('pengguna_tiket', 'Kembali', $arrayName = array('class' => 'btn btn-default',));?>
		</div>
	</div>
</div>