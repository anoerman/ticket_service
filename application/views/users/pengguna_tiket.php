<div class="container">
	<div class="col-lg-12">
		<h2>Pengguna Tiket</h2>

		<div class="row">
			<div class="col-sm-12">
				<div class="btn-group btn-group-sm pull-right">
					<a href="<?php echo base_url('pengguna_tiket/tambah') ?>" class="btn btn-primary">Pengguna Baru</a>
				</div>
				<p>Berikut adalah daftar pengguna yang telah terdaftar.</p>
			</div>
			<div class="clearfix">&nbsp;</div>
		</div>
		
		<?php echo $message;?>
		
		<div class="table-responsive">
			<table class="table table-striped table-bordered datatables">
				<thead>
					<tr>
						<th>Username</th>
						<th>Nama</th>
						<th>Email</th>
						<th>Grup</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<?php foreach ($users as $user):?>
					<tr>
			            <td><?php echo htmlspecialchars($user->username,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?> <?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')."<br>" ;?>
			                <?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("pengguna_tiket/deactivate/".$user->id, 'Aktif', array('class' => 'btn btn-sm btn-success', )) : anchor("pengguna_tiket/activate/". $user->id, 'Inaktif', array('class' => 'btn btn-sm btn-danger', ));?></td>
						<td><?php echo anchor("pengguna_tiket/ubah/".$user->id, 'Ubah', array('class' => 'btn btn-sm btn-primary', )) . " " . anchor("pengguna_tiket/detail/".$user->id, 'Detail', array('class' => 'btn btn-sm btn-default', )) ;?></td>
					</tr>
				<?php endforeach;?>
			</table>
		</div>
		
	</div>
</div>