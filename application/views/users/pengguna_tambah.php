<div class="container">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2>Pengguna</h2>
		<p>Masukkan detail pengguna baru</p>
		<hr>
		<?php echo $message;?>
	</div>
	<?php echo form_open("pengguna_tiket/tambah", array('autocomplete' => 'off'));?>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-group">
			<?php echo lang('create_user_fname_label', 'first_name');?> <br />
			<?php //echo form_error('first_name');?>
			<?php echo form_input($first_name);?>
		</div>

		<div class="form-group">
			<?php echo lang('create_user_lname_label', 'last_name');?> <br />
			<?php //echo form_error('last_name');?>
			<?php echo form_input($last_name);?>
		</div>
		
		<?php
		if($identity_column!=='email') {
			echo '<div class="form-group">';
			echo lang('create_user_identity_label', 'identity');
			echo '<br />';
			//echo form_error('identity');
			echo form_input($identity);
			echo '</div>';
		}
		?>

		<div class="form-group">
			<?php echo lang('create_user_email_label', 'email');?> <br />
			<?php //echo form_error('email');?>
			<?php echo form_input($email);?>
		</div>

		<div class="form-group">
			<?php echo lang('create_user_password_label', 'password');?> <br />
			<?php //echo form_error('password');?>
			<?php echo form_input($password);?>
		</div>

		<div class="form-group">
			<?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
			<?php //echo form_error('password_confirm');?>
			<?php echo form_input($password_confirm);?>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-group">
			<label for="jabatan" class="control-label">Jabatan</label> <br />
			<select name="jabatan" id="jabatan" class="form-control">
				<option value="">Pilih Jabatan</option>
			<?php foreach ($jabatan_data->result() as $jabatan): ?>
				<option value="<?php echo $jabatan->id ?>"><?php echo $jabatan->nama ?></option>
			<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
			<label for="status_pegawai" class="control-label">Status Pegawai</label> <br />
			<select name="status_pegawai" id="status_pegawai" class="form-control">
				<option value="">Pilih Status Pegawai</option>
			<?php foreach ($status_pegawai_data->result() as $status_pegawai): ?>
				<option value="<?php echo $status_pegawai->id ?>"><?php echo $status_pegawai->nama ?></option>
			<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
			<label for="unit_kerja" class="control-label">Unit Kerja</label> <br />
			<select name="unit_kerja" id="unit_kerja" class="form-control">
				<option value="">Pilih Unit Kerja</option>
			<?php foreach ($unit_kerja_data->result() as $unit_kerja): ?>
				<option value="<?php echo $unit_kerja->id ?>"><?php echo $unit_kerja->nama ?></option>
			<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
			<label for="lokasi_kerja" class="control-label">Lokasi Kerja</label> <br />
			<select name="lokasi_kerja" id="lokasi_kerja" class="form-control">
				<option value="">Pilih Lokasi Kerja</option>
			<?php foreach ($lokasi_kerja_data->result() as $lokasi_kerja): ?>
				<option value="<?php echo $lokasi_kerja->id ?>"><?php echo $lokasi_kerja->nama ?></option>
			<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
			<label for="gedung" class="control-label">Gedung</label> <br />
			<select name="gedung" id="gedung" class="form-control">
				<option value="">Pilih Gedung</option>
			<?php foreach ($gedung_data->result() as $gedung): ?>
				<option value="<?php echo $gedung->id ?>"><?php echo $gedung->nama ?></option>
			<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
			<label for="ruang" class="control-label">Ruang</label> <br />
			<select name="ruang" id="ruang" class="form-control">
				<option value="">Pilih Ruang</option>
			<?php foreach ($ruang_data->result() as $ruang): ?>
				<option value="<?php echo $ruang->id ?>"><?php echo $ruang->nama ?></option>
			<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
			<label for="lantai" class="control-label">Lantai</label> <br />
			<input type="number" name="lantai" id="lantai" class="form-control" placeholder="Lantai" value="1">
		</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group text-center">
			<hr>
			<?php echo form_submit('submit', lang('create_user_submit_btn'), $arrayName = array('class' => 'btn btn-primary',));?>
			<?php echo anchor('pengguna_tiket', 'Kembali', $arrayName = array('class' => 'btn btn-default',));?>
		</div>
	</div>
	<?php echo form_close();?>
</div>