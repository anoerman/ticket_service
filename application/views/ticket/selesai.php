<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
			<div class="panel panel-default">
				<div class="panel-body">
					<h2>Tiket - Selesai</h2>
					<div><?php echo $message; ?></div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Tanggal</th>
									<th>Oleh</th>
									<th>Jenis Tiket</th>
									<th>Isi Tiket</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_tiket->result() as $tiket): ?>
								<tr>
									<td><?php echo $tiket->kode ?></td>
									<td><?php echo date("d M Y",$tiket->tanggal) . "<br>" . date("H:i:s",$tiket->tanggal) ?></td>
									<td><?php echo $tiket->first_name . " " . $tiket->last_name ?></td>
									<td><?php echo $tiket->nama ?></td>
									<td><?php echo $tiket->isi ?></td>
									<td>
										<div class="btn-group-vertical">
											<a class="btn btn-sm btn-default" href="<?php echo base_url('tiket/detail/selesai/'.$tiket->tiket_id) ?>" role="button" title="Detail"><i class="glyphicon glyphicon-eye-open"></i> Detail</a>
											<?php if ($this->ion_auth->is_admin()) : ?>
											<a class="btn btn-sm btn-primary" href="<?php echo base_url('tiket/ubah/selesai/'.$tiket->tiket_id) ?>" role="button" title="Ubah"><i class="glyphicon glyphicon-edit"></i> Ubah</a>
											<a class="btn btn-sm btn-danger" href="<?php echo base_url('tiket/hapus/selesai/'.$tiket->tiket_id) ?>" role="button" title="Hapus"><i class="glyphicon glyphicon-trash"></i> Hapus</a>
											<?php endif ?>
										</div>
									</td>
								</tr>
								<?php endforeach ?>
								<?php if (count($data_tiket->result()) == 0): ?>
									<tr class="text-center">
										<td colspan="6">No Data</td>
									</tr>
								<?php endif ?>
							</tbody>
						</table>

						<?php echo $pagination ?>
					</div>
				</div>
			</div>
		</div>
		<!-- Menu -->
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
			<div class="list-group">
				<a class="list-group-item" href="<?php echo base_url("tiket") ?>" ><i class="glyphicon glyphicon-dashboard"></i> &nbsp; Beranda</a>
				<?php if ($this->ion_auth->logged_in()) :?>
				<a class="list-group-item" href="<?php echo base_url("tiket/tambah") ?>" ><i class="glyphicon glyphicon-plus"></i> &nbsp; Tiket Baru</a>
				<?php endif ?>
				<a class="list-group-item" href="<?php echo base_url("tiket/daftar_tunggu") ?>" ><i class="glyphicon glyphicon-list"></i> &nbsp; Daftar Tunggu</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/proses") ?>" ><i class="glyphicon glyphicon-refresh"></i> &nbsp; Dalam Proses</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/tertunda") ?>" ><i class="glyphicon glyphicon-time"></i> &nbsp; Tertunda</a>
				<a class="list-group-item active" href="<?php echo base_url("tiket/selesai") ?>" ><i class="glyphicon glyphicon-check"></i> &nbsp; Selesai</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/tidak_selesai") ?>" ><i class="glyphicon glyphicon-remove"></i> &nbsp; Tidak Selesai</a>
			</div>
		</div>

	</div>
</div>