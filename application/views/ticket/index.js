
<script language="javascript">

	$(document).ready(function() {
		load_statistik();
	});
	
	function load_statistik () {
		$.ajax({
			url: '<?php echo base_url('tiket/statistik') ?>',
			type: 'GET',
			dataType: 'html',
			
		})
		.done(function(data) {
			$("#statistik").html(data);
			console.log("Statistik berhasil ditampilkan.");
		})
		.fail(function() {
			console.log("Error menampilkan data Statistik.");
		});
	}
	
	// Auto reload content
	setInterval(function(){load_statistik();}, <?php echo $time_refresh ?>);

</script>