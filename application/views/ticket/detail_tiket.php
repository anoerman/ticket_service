<div class="container">
	<div class="row">

		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
			<div class="panel panel-default">
				<div class="panel-body">
					<h2>Tiket Detail</h2>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<?php echo $message; ?>
							
							<div class="form-group form-horizontal">
								<label for="kode" class="control-label col-lg-2">Kode :</label>
								<div class="col-lg-10">
									<p class="form-control-static"><?php echo $detail_tiket->kode; ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="jenis_tiket" class="control-label col-lg-2">Jenis Tiket :</label>
								<div class="col-lg-10">
									<p class="form-control-static"><?php echo $detail_tiket->nama_jenis_tiket; ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="tanggal" class="control-label col-lg-2">Tanggal Tiket :</label>
								<div class="col-lg-10">
									<p class="form-control-static"><?php echo date('d F Y (H:i:s)', $detail_tiket->tanggal); ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="isi" class="control-label col-lg-2">Isi Tiket :</label>
								<div class="col-lg-10">
									<textarea name="isi" id="isi" class="form-control" style="resize: vertical;" rows="4" readonly=""><?php echo $detail_tiket->isi ?></textarea>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="form-group form-horizontal">
								<label for="oleh" class="control-label col-lg-4">Tiket oleh :</label>
								<div class="col-lg-8">
									<p class="form-control-static"><?php echo $detail_tiket->fn_oleh . " " . $detail_tiket->ln_oleh; ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="jabatan_id" class="control-label col-lg-4">Jabatan :</label>
								<div class="col-lg-8">
									<p class="form-control-static"><?php echo $detail_tiket->nama_jabatan; ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="status_pegawai_id" class="control-label col-lg-4">Status :</label>
								<div class="col-lg-8">
									<p class="form-control-static"><?php echo $detail_tiket->nama_status_pegawai; ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="unit_kerja_id" class="control-label col-lg-4">Unit Kerja :</label>
								<div class="col-lg-8">
									<p class="form-control-static"><?php echo $detail_tiket->nama_unit_kerja; ?></p>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="form-group form-horizontal">
								<label for="lokasi_kerja_id" class="control-label col-lg-4">Lokasi Kerja :</label>
								<div class="col-lg-8">
									<p class="form-control-static"><?php echo $detail_tiket->nama_lokasi_kerja; ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="gedung_id" class="control-label col-lg-4">Gedung :</label>
								<div class="col-lg-8">
									<p class="form-control-static"><?php echo $detail_tiket->nama_gedung; ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="ruang_id" class="control-label col-lg-4">Ruang :</label>
								<div class="col-lg-8">
									<p class="form-control-static"><?php echo $detail_tiket->nama_ruang; ?></p>
								</div>
							</div>

							<div class="form-group form-horizontal">
								<label for="lantai" class="control-label col-lg-4">Lantai :</label>
								<div class="col-lg-8">
									<p class="form-control-static"><?php echo $detail_tiket->lantai; ?></p>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<hr>
							<h3>Penanganan</h3>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group form-horizontal">
								<label for="status" class="control-label col-lg-2">Status :</label>
								<div class="col-lg-10">
									<p class="form-control-static"><?php echo ucwords($detail_tiket->status); ?></p>
								</div>
							</div>
							<div class="form-group form-horizontal">
								<label for="selesai_oleh" class="control-label col-lg-2">Oleh :</label>
								<div class="col-lg-10">
									<p class="form-control-static"><?php echo $detail_tiket->fn_selesai_oleh . " " . $detail_tiket->ln_selesai_oleh; ?></p>
								</div>
							</div>
							<div class="form-group form-horizontal">
								<label for="selesai_tanggal" class="control-label col-lg-2">Tanggal :</label>
								<div class="col-lg-10">
									<p class="form-control-static"><?php echo ($detail_tiket->selesai_tanggal!=0) ? date('d F Y (H:i:s)', $detail_tiket->selesai_tanggal) : "&nbsp;"; ?></p>
								</div>
							</div>
							<div class="form-group form-horizontal">
								<label for="penanganan" class="control-label col-lg-2">Penanganan :</label>
								<div class="col-lg-10">
									<textarea name="penanganan" id="penanganan" class="form-control" style="resize: vertical;" rows="4" readonly=""><?php echo $detail_tiket->penanganan ?></textarea>
								</div>
							</div>
							<hr>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
							<hr>
							<a class="btn btn-default" href="<?php echo base_url('tiket/'.$halaman) ?>" role="button">Kembali</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Menu -->
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
			<div class="list-group">
				<a class="list-group-item" href="<?php echo base_url("tiket") ?>" ><i class="glyphicon glyphicon-dashboard"></i> &nbsp; Beranda</a>
				<?php if ($this->ion_auth->logged_in()) :?>
				<a class="list-group-item" href="<?php echo base_url("tiket/tambah") ?>" ><i class="glyphicon glyphicon-plus"></i> &nbsp; Tiket Baru</a>
				<?php endif ?>
				<a class="list-group-item <?php echo ($halaman=='daftar_tunggu') ? 'active' : '' ?>" href="<?php echo base_url("tiket/daftar_tunggu") ?>" ><i class="glyphicon glyphicon-list"></i> &nbsp; Daftar Tunggu</a>
				<a class="list-group-item <?php echo ($halaman=='proses') ? 'active' : '' ?>" href="<?php echo base_url("tiket/proses") ?>" ><i class="glyphicon glyphicon-refresh"></i> &nbsp; Dalam Proses</a>
				<a class="list-group-item <?php echo ($halaman=='tertunda') ? 'active' : '' ?>" href="<?php echo base_url("tiket/tertunda") ?>" ><i class="glyphicon glyphicon-time"></i> &nbsp; Tertunda</a>
				<a class="list-group-item <?php echo ($halaman=='selesai') ? 'active' : '' ?>" href="<?php echo base_url("tiket/selesai") ?>" ><i class="glyphicon glyphicon-check"></i> &nbsp; Selesai</a>
				<a class="list-group-item <?php echo ($halaman=='tidak_selesai') ? 'active' : '' ?>" href="<?php echo base_url("tiket/tidak_selesai") ?>" ><i class="glyphicon glyphicon-remove"></i> &nbsp; Tidak Selesai</a>
			</div>
		</div>

	</div>
</div>
