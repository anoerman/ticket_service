
<script language="javascript">

	$(document).ready(function() {
		load_isi_data();
	});
	
	function load_isi_data () {
		$.ajax({
			url: '<?php echo base_url('tiket/daftar_tunggu_isi') ?>',
			type: 'GET',
			dataType: 'html',
			
		})
		.done(function(data) {
			$("#isi").html(data);
			console.log("Tiket berhasil ditampilkan.");
		})
		.fail(function() {
			console.log("Error menampilkan data tiket.");
		});
	}
	
	// Auto reload content
	setInterval(function(){load_isi_data();}, <?php echo $time_refresh ?>);

</script>