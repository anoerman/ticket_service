<?php 
echo $message;

// Set notification sound if notification says yes
if ($notification == "yes") {
	echo '<audio autoplay="autoplay"><source src="' .$sound_path. '.mp3" type="audio/mpeg" /><source src="' .$sound_path. '.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="' .$sound_path. '.mp3" /></audio>';

	echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Informasi : </strong> Ada tiket baru telah masuk! Harap segera dilakukan pengecekkan.</div></div>';
}
$this->session->set_userdata('notification', $total_tiket);
?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	<div class="thumbnail">
		<h3 class="text-center">Statistik Tiket Minggu Ini</h3>
		<canvas id="ChartMingguIni"></canvas>
		<div class="caption">
			<!-- <hr> -->
			<div class="well well-sm">
				<style> ul { list-style-type: none; padding: 0px; margin: 0px; } </style>
				<ul>
					<li> <span style="background-color:#ff6384; color:#ff6384; "> __ </span> &nbsp;&nbsp; <?php echo date('d F', $hari_ke_6) ?> &nbsp; = &nbsp; <?php echo $n_hari_ke_6 ?> Tiket</li>
					<li> <span style="background-color:#36a2eb; color:#36a2eb; "> __ </span> &nbsp;&nbsp; <?php echo date('d F', $hari_ke_5) ?> &nbsp; = &nbsp; <?php echo $n_hari_ke_5 ?> Tiket</li>
					<li> <span style="background-color:#ffce56; color:#ffce56; "> __ </span> &nbsp;&nbsp; <?php echo date('d F', $hari_ke_4) ?> &nbsp; = &nbsp; <?php echo $n_hari_ke_4 ?> Tiket</li>
					<li> <span style="background-color:#4bc0c0; color:#4bc0c0; "> __ </span> &nbsp;&nbsp; <?php echo date('d F', $hari_ke_3) ?> &nbsp; = &nbsp; <?php echo $n_hari_ke_3 ?> Tiket</li>
					<li> <span style="background-color:#9969ff; color:#9969ff; "> __ </span> &nbsp;&nbsp; <?php echo date('d F', $hari_ke_2) ?> &nbsp; = &nbsp; <?php echo $n_hari_ke_2 ?> Tiket</li>
					<li> <span style="background-color:#ff9f40; color:#ff9f40; "> __ </span> &nbsp;&nbsp; <?php echo date('d F', $hari_ke_1) ?> &nbsp; = &nbsp; <?php echo $n_hari_ke_1 ?> Tiket</li>
					<li> <span style="background-color:#bb9f7b; color:#bb9f7b; "> __ </span> &nbsp;&nbsp; <?php echo date('d F', $sekarang) ?> &nbsp; = &nbsp; <?php echo $n_sekarang ?> Tiket</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	<div class="thumbnail">
		<h3 class="text-center">Statistik Tiket Keseluruhan</h3>
		<canvas id="ChartKeseluruhan"></canvas>
	</div>
</div>

<script src="<?php echo base_url('assets/js/Chart.bundle.min.js'); ?>"></script>

<script>
	var dataMingguan = {
		labels: [
			"<?php echo date('d M', $hari_ke_6) ?>", 
			"<?php echo date('d M', $hari_ke_5) ?>", 
			"<?php echo date('d M', $hari_ke_4) ?>", 
			"<?php echo date('d M', $hari_ke_3) ?>", 
			"<?php echo date('d M', $hari_ke_2) ?>", 
			"<?php echo date('d M', $hari_ke_1) ?>", 
			"<?php echo date('d M', $sekarang) ?>", 
		],
		datasets: [
			{
				label: "Tiket 7 Hari Terakhir",
				backgroundColor: [
					'rgba(255, 99, 132, 0.8)',
					'rgba(54, 162, 235, 0.8)',
					'rgba(255, 206, 86, 0.8)',
					'rgba(75, 192, 192, 0.8)',
					'rgba(153, 102, 255, 0.8)',
					'rgba(255, 159, 64, 0.8)',
					'rgba(187, 159, 123, 0.8)',
				],
				borderColor: [
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)',
					'rgba(187, 159, 123, 1)',
				],
				borderWidth: 1,
				data: [
					<?php echo $n_hari_ke_6 ?>, 
					<?php echo $n_hari_ke_5 ?>, 
					<?php echo $n_hari_ke_4 ?>, 
					<?php echo $n_hari_ke_3 ?>, 
					<?php echo $n_hari_ke_2 ?>, 
					<?php echo $n_hari_ke_1 ?>, 
					<?php echo $n_sekarang ?>
				],
			}
		]
	};
	var ctxmingguini = $("#ChartMingguIni");
	var ChartMingguIni = new Chart(ctxmingguini, {
		type: 'bar',
		// type: 'horizontalBar',
		data: dataMingguan,
		options: {
			animation:{
				animateScale:true
			},
			legend: {
				display: false,
			}
		}
	});
</script>

<script>
	var dataKeseluruhan = {
		labels: [
			"Daftar Tunggu",
			"Proses",
			"Tertunda",
			"Selesai",
			"Tidak Selesai",
		],
		datasets: [
		{
			data: [
				<?php echo $n_daftar_tunggu ?>, 
				<?php echo $n_proses ?>, 
				<?php echo $n_tertunda ?>, 
				<?php echo $n_selesai ?>,
				<?php echo $n_tidak_selesai ?>, 
			],
			backgroundColor: [
				"#534B52",
				"#59758E",
				"#FFCE56",
				"#06D6A0",
				"#FF6384",
			],
			hoverBackgroundColor: [
				"#635B62",
				"#69859E",
				"#EFDE66",
				"#16E6A0",
				"#EF7394",
			]
		}]
	};

	var ctx = $("#ChartKeseluruhan");
	var ChartKeseluruhan = new Chart(ctx, {
		type: 'pie',
		data: dataKeseluruhan,
		options: {
			animation:{
				animateScale:true
			},
			legend: {
				display: true,
				position: 'bottom',
			}
		}
	});
</script>