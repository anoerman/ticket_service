<div class="container">
	<div class="row">

		<!-- Form -->
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
			<div class="panel panel-default">
				<div class="panel-body">
					<h2>Tambah Tiket</h2>
					<form action="<?php echo base_url('tiket/tambah') ?>" method="post">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<?php echo $message; ?>

								<div class="form-group">
									<label for="kode" class="control-label">Kode :</label>
									<input class="form-control" value="<?php echo $kode; ?>" disabled>
									<input type="hidden" name="kode" id="kode" value="<?php echo $kode ?>">
								</div>

								<div class="form-group">
									<label for="jenis_tiket" class="control-label">Jenis Tiket :</label>
									<select name="jenis_tiket" id="jenis_tiket" class="form-control">
										<option value="">Pilih Jenis Tiket</option>
									<?php foreach ($jenis_tiket->result() as $jenis_tiket): ?>
										<option value="<?php echo $jenis_tiket->id ?>" <?php echo ($jenis_tiket->id == $jenis_tiket_val) ? "selected" : ""; ?>><?php echo $jenis_tiket->nama ?></option>
									<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label for="isi" class="control-label">Isi Tiket :</label>
									<textarea name="isi" id="isi" class="form-control" style="resize: vertical; max-height: 150px;" rows="4" required><?php echo $isi_val ?></textarea>
									<p class="help-block">Harap isikan tiket sesuai dengan permasalahan yang terjadi dengan jelas.</p>
								</div>

								<legend>Detail Pembuat Tiket</legend>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group form-horizontal">
									<label for="oleh" class="control-label col-lg-4">Tiket oleh :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $user->first_name . " " . $user->last_name; ?></p>
										<input type="hidden" name="oleh" id="oleh" value="<?php echo $this->session->userdata('identity'); ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="jabatan_id" class="control-label col-lg-4">Jabatan :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $jabatan_sekarang->nama; ?></p>
										<input type="hidden" name="jabatan_id" id="jabatan_id" value="<?php echo $jabatan_sekarang->jabatan_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="status_pegawai_id" class="control-label col-lg-4">Status :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $status_pegawai_sekarang->nama; ?></p>
										<input type="hidden" name="status_pegawai_id" id="status_pegawai_id" value="<?php echo $status_pegawai_sekarang->status_pegawai_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="unit_kerja_id" class="control-label col-lg-4">Unit Kerja :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $unit_kerja_sekarang->nama; ?></p>
										<input type="hidden" name="unit_kerja_id" id="unit_kerja_id" value="<?php echo $unit_kerja_sekarang->unit_kerja_id ?>">
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<?php if ($this->ion_auth->is_admin()): ?>
								<div class="form-group">
									<label for="lokasi_kerja_id" class="control-label">Lokasi Kerja :</label> <br />
									<select name="lokasi_kerja_id" id="lokasi_kerja_id" class="form-control">
										<option value="">Pilih Lokasi Kerja</option>
									<?php foreach ($lokasi_kerja_data->result() as $lokasi_kerja): ?>
										<option value="<?php echo $lokasi_kerja->id ?>" <?php echo ($lokasi_kerja->id == $lokasi_kerja_sekarang->lokasi_kerja_id) ? "selected" : ""; ?>><?php echo $lokasi_kerja->nama ?></option>
									<?php endforeach ?>
									</select>
								</div>
								
								<div class="form-group">
									<label for="gedung_id" class="control-label">Gedung :</label> <br />
									<select name="gedung_id" id="gedung_id" class="form-control">
										<option value="">Pilih Gedung</option>
									<?php foreach ($gedung_data->result() as $gedung): ?>
										<option value="<?php echo $gedung->id ?>" <?php echo ($gedung->id == $gedung_sekarang->gedung_id) ? "selected" : ""; ?>><?php echo $gedung->nama ?></option>
									<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label for="ruang_id" class="control-label">Ruang :</label> <br />
									<select name="ruang_id" id="ruang_id" class="form-control">
										<option value="">Pilih Ruang</option>
									<?php foreach ($ruang_data->result() as $ruang): ?>
										<option value="<?php echo $ruang->id ?>"<?php echo ($ruang->id == $ruang_sekarang->ruang_id) ? "selected" : ""; ?>><?php echo $ruang->nama ?></option>
									<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label for="lantai" class="control-label">Lantai :</label> <br />
									<input type="number" name="lantai" id="lantai" class="form-control" placeholder="Lantai" value="<?php echo $lantai_sekarang->lantai ?>">
								</div>
								<?php else: ?>
								<div class="form-group form-horizontal">
									<label for="lokasi_kerja_id" class="control-label col-lg-4">Lokasi Kerja :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $lokasi_kerja_sekarang->nama; ?></p>
										<input type="hidden" name="lokasi_kerja_id" id="lokasi_kerja_id" value="<?php echo $lokasi_kerja_sekarang->lokasi_kerja_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="gedung_id" class="control-label col-lg-4">Gedung :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $gedung_sekarang->nama; ?></p>
										<input type="hidden" name="gedung_id" id="gedung_id" value="<?php echo $gedung_sekarang->gedung_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="ruang_id" class="control-label col-lg-4">Ruang :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $ruang_sekarang->nama; ?></p>
										<input type="hidden" name="ruang_id" id="ruang_id" value="<?php echo $ruang_sekarang->ruang_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="lantai" class="control-label col-lg-4">Lantai :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $lantai_sekarang->lantai; ?></p>
										<input type="hidden" name="lantai" id="lantai" value="<?php echo $lantai_sekarang->lantai ?>">
									</div>
								</div>
								<?php endif ?>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
								<hr>
								<button type="submit" class="btn btn-primary" onclick="return confirm('Pastikan semua data sudah terisi dengan jelas.\nSimpan data tiket ini?')">Simpan Tiket</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Menu -->
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
			<div class="list-group">
				<a class="list-group-item" href="<?php echo base_url("tiket") ?>" ><i class="glyphicon glyphicon-dashboard"></i> &nbsp; Beranda</a>
				<?php if ($this->ion_auth->logged_in()) :?>
				<a class="list-group-item" href="<?php echo base_url("tiket/tambah") ?>" ><i class="glyphicon glyphicon-plus"></i> &nbsp; Tiket Baru</a>
				<?php endif ?>
				<a class="list-group-item" href="<?php echo base_url("tiket/daftar_tunggu") ?>" ><i class="glyphicon glyphicon-list"></i> &nbsp; Daftar Tunggu</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/proses") ?>" ><i class="glyphicon glyphicon-refresh"></i> &nbsp; Dalam Proses</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/tertunda") ?>" ><i class="glyphicon glyphicon-time"></i> &nbsp; Tertunda</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/selesai") ?>" ><i class="glyphicon glyphicon-check"></i> &nbsp; Selesai</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/tidak_selesai") ?>" ><i class="glyphicon glyphicon-remove"></i> &nbsp; Tidak Selesai</a>
			</div>
		</div>

	</div>
</div>