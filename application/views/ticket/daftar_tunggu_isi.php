<?php 
echo $message;

// Set notification sound if notification says yes
if ($notification == "yes") {
	echo '<audio autoplay="autoplay"><source src="' .$sound_path. '.mp3" type="audio/mpeg" /><source src="' .$sound_path. '.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="' .$sound_path. '.mp3" /></audio>';

	echo '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Informasi : </strong> Ada tiket baru telah masuk! Harap segera dilakukan pengecekkan.</div>';
}
$this->session->set_userdata('notification', $total_tiket);
?>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Kode</th>
			<th>Tanggal</th>
			<th>Oleh</th>
			<th>Jenis Tiket</th>
			<th>Isi Tiket</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data_tiket->result() as $tiket): ?>
		<tr>
			<td><?php echo $tiket->kode ?></td>
			<td><?php echo date("d M Y",$tiket->tanggal) . "<br>" . date("H:i:s",$tiket->tanggal) ?></td>
			<td><?php echo $tiket->first_name . " " . $tiket->last_name ?></td>
			<td><?php echo $tiket->nama ?></td>
			<td><?php echo $tiket->isi ?></td>
			<td>
				<div class="btn-group-vertical">
					<a class="btn btn-sm btn-default" href="<?php echo base_url('tiket/detail/daftar_tunggu/'.$tiket->tiket_id) ?>" role="button" title="Detail"><i class="glyphicon glyphicon-eye-open"></i> Detail</a>
					<?php if ($this->ion_auth->is_admin()) : ?>
					<a class="btn btn-sm btn-primary" href="<?php echo base_url('tiket/ubah/daftar_tunggu/'.$tiket->tiket_id) ?>" role="button" title="Ubah"><i class="glyphicon glyphicon-edit"></i> Ubah</a>
					<a class="btn btn-sm btn-danger" href="<?php echo base_url('tiket/hapus/daftar_tunggu/'.$tiket->tiket_id) ?>" role="button" title="Hapus"><i class="glyphicon glyphicon-trash"></i> Hapus
					</a>
					<?php endif ?>
				</div>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>