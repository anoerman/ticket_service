<div class="container">
	<div class="row">

		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
			<div class="panel panel-default">
				<div class="panel-body">
					<h2>Ubah Tiket</h2>
					<form action="<?php echo base_url('tiket/ubah/'.$halaman.'/'.$tiket_id) ?>" method="post">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<?php echo $message; ?>

								<div class="form-group">
									<label for="kode" class="control-label">Kode :</label>
									<input class="form-control" value="<?php echo $kode; ?>" disabled>
									<input type="hidden" name="kode" id="kode" value="<?php echo $kode ?>">
								</div>

								<div class="form-group">
									<label for="tanggal" class="control-label">Tanggal :</label> <br />
									<input type="text" name="tanggal" id="tanggal" class="form-control tanggal" value="<?php echo $tanggal_val ?>">
								</div>

								<div class="form-group">
									<label for="jenis_tiket" class="control-label">Jenis Tiket :</label>
									<select name="jenis_tiket" id="jenis_tiket" class="form-control">
										<option value="">Pilih Jenis Tiket</option>
									<?php foreach ($jenis_tiket->result() as $jenis_tiket): ?>
										<option value="<?php echo $jenis_tiket->id ?>" <?php echo ($jenis_tiket->id == $jenis_tiket_val) ? "selected" : ""; ?>><?php echo $jenis_tiket->nama ?></option>
									<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label for="isi" class="control-label">Isi Tiket :</label>
									<textarea name="isi" id="isi" class="form-control" style="resize: vertical; max-height: 150px;" rows="4" required><?php echo $isi_val ?></textarea>
									<p class="help-block">Harap isikan tiket sesuai dengan permasalahan yang terjadi dengan jelas.</p>
								</div>

								<legend>Detail Pembuat Tiket</legend>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group form-horizontal">
									<label for="oleh" class="control-label col-lg-4">Tiket oleh :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $detail_tiket->fn_oleh . " " . $detail_tiket->ln_oleh; ?></p>
										<input type="hidden" name="oleh" id="oleh" value="<?php echo $detail_tiket->oleh; ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="jabatan_id" class="control-label col-lg-4">Jabatan :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $detail_tiket->nama_jabatan; ?></p>
										<input type="hidden" name="jabatan_id" id="jabatan_id" value="<?php echo $detail_tiket->jabatan_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="status_pegawai_id" class="control-label col-lg-4">Status :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $detail_tiket->nama_status_pegawai; ?></p>
										<input type="hidden" name="status_pegawai_id" id="status_pegawai_id" value="<?php echo $detail_tiket->status_pegawai_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="unit_kerja_id" class="control-label col-lg-4">Unit Kerja :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $detail_tiket->nama_unit_kerja; ?></p>
										<input type="hidden" name="unit_kerja_id" id="unit_kerja_id" value="<?php echo $detail_tiket->unit_kerja_id ?>">
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<?php if ($this->ion_auth->is_admin()): ?>
								<div class="form-group">
									<label for="lokasi_kerja_id" class="control-label">Lokasi Kerja :</label> <br />
									<select name="lokasi_kerja_id" id="lokasi_kerja_id" class="form-control">
										<option value="">Pilih Lokasi Kerja</option>
									<?php foreach ($lokasi_kerja_data->result() as $lokasi_kerja): ?>
										<option value="<?php echo $lokasi_kerja->id ?>" <?php echo ($lokasi_kerja->id == $detail_tiket->lokasi_kerja_id) ? "selected" : ""; ?>><?php echo $lokasi_kerja->nama ?></option>
									<?php endforeach ?>
									</select>
								</div>
								
								<div class="form-group">
									<label for="gedung_id" class="control-label">Gedung :</label> <br />
									<select name="gedung_id" id="gedung_id" class="form-control">
										<option value="">Pilih Gedung</option>
									<?php foreach ($gedung_data->result() as $gedung): ?>
										<option value="<?php echo $gedung->id ?>" <?php echo ($gedung->id == $detail_tiket->gedung_id) ? "selected" : ""; ?>><?php echo $gedung->nama ?></option>
									<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label for="ruang_id" class="control-label">Ruang :</label> <br />
									<select name="ruang_id" id="ruang_id" class="form-control">
										<option value="">Pilih Ruang</option>
									<?php foreach ($ruang_data->result() as $ruang): ?>
										<option value="<?php echo $ruang->id ?>"<?php echo ($ruang->id == $detail_tiket->ruang_id) ? "selected" : ""; ?>><?php echo $ruang->nama ?></option>
									<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label for="lantai" class="control-label">Lantai :</label> <br />
									<input type="number" name="lantai" id="lantai" class="form-control" placeholder="Lantai" value="<?php echo $detail_tiket->lantai ?>">
								</div>
								<?php else: ?>
								<div class="form-group form-horizontal">
									<label for="lokasi_kerja_id" class="control-label col-lg-4">Lokasi Kerja :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $detail_tiket->nama_lokasi_kerja; ?></p>
										<input type="hidden" name="lokasi_kerja_id" id="lokasi_kerja_id" value="<?php echo $detail_tiket->lokasi_kerja_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="gedung_id" class="control-label col-lg-4">Gedung :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $nama_gedung; ?></p>
										<input type="hidden" name="gedung_id" id="gedung_id" value="<?php echo $detail_tiket->gedung_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="ruang_id" class="control-label col-lg-4">Ruang :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $nama_ruang; ?></p>
										<input type="hidden" name="ruang_id" id="ruang_id" value="<?php echo $detail_tiket->ruang_id ?>">
									</div>
								</div>

								<div class="form-group form-horizontal">
									<label for="lantai" class="control-label col-lg-4">Lantai :</label>
									<div class="col-lg-8">
										<p class="form-control-static"><?php echo $detail_tiket->lantai; ?></p>
										<input type="hidden" name="lantai" id="lantai" value="<?php echo $detail_tiket->lantai ?>">
									</div>
								</div>
								<?php endif ?>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<legend>Detail Penanganan Tiket</legend>

								<div class="form-group">
									<label for="status" class="control-label">Status :</label> <br />
									<select name="status" id="status" class="form-control">
										<option value="">Pilih Status</option>
										<option value="daftar tunggu" <?php echo ($detail_tiket->status=="daftar tunggu") ? "selected" : ""; ?>>Daftar Tunggu</option>
										<option value="proses" <?php echo ($detail_tiket->status=="proses") ? "selected" : ""; ?>>Dalam Proses</option>
										<option value="tertunda" <?php echo ($detail_tiket->status=="tertunda") ? "selected" : ""; ?>>Tertunda</option>
										<option value="selesai" <?php echo ($detail_tiket->status=="selesai") ? "selected" : ""; ?>>Selesai</option>
										<option value="tidak selesai" <?php echo ($detail_tiket->status=="tidak selesai") ? "selected" : ""; ?>>Tidak Selesai</option>
									</select>
								</div>

								<div class="form-group">
									<label for="selesai_oleh" class="control-label">Penanganan Oleh :</label> <br />
									<select name="selesai_oleh" id="selesai_oleh" class="form-control">
										<option value="">Pilih Karyawan</option>
									<?php foreach ($selesai_oleh->result() as $so): ?>
										<option value="<?php echo $so->username ?>"<?php echo ($so->username == $detail_tiket->selesai_oleh) ? "selected" : ""; ?>><?php echo $so->first_name . " " . $so->last_name; ?></option>
									<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label for="selesai_tanggal" class="control-label">Penanganan Tanggal :</label> <br />
									<input type="text" name="selesai_tanggal" id="selesai_tanggal" class="form-control tanggal" value="<?php echo $selesai_tanggal_val ?>">
								</div>

								<div class="form-group">
									<label for="penanganan" class="control-label">Penanganan Tiket :</label>
									<textarea name="penanganan" id="penanganan" class="form-control" style="resize: vertical; max-height: 150px;" rows="4" required><?php echo $penanganan_val ?></textarea>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
								<hr>
								<button type="submit" class="btn btn-primary" onclick="return confirm('Pastikan semua data sudah terisi dengan jelas.\nSimpan data tiket ini?')">Simpan Tiket</button>
								<a class="btn btn-default" href="<?php echo base_url('tiket/'.$halaman) ?>" role="button">Kembali</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Menu -->
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
			<div class="list-group">
				<a class="list-group-item" href="<?php echo base_url("tiket") ?>" ><i class="glyphicon glyphicon-dashboard"></i> &nbsp; Beranda</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/tambah") ?>" ><i class="glyphicon glyphicon-plus"></i> &nbsp; Tiket Baru</a>
				<a class="list-group-item <?php echo ($halaman=='daftar_tunggu') ? 'active' : '' ?>" href="<?php echo base_url("tiket/daftar_tunggu") ?>" ><i class="glyphicon glyphicon-list"></i> &nbsp; Daftar Tunggu</a>
				<a class="list-group-item <?php echo ($halaman=='proses') ? 'active' : '' ?>" href="<?php echo base_url("tiket/proses") ?>" ><i class="glyphicon glyphicon-refresh"></i> &nbsp; Dalam Proses</a>
				<a class="list-group-item <?php echo ($halaman=='tertunda') ? 'active' : '' ?>" href="<?php echo base_url("tiket/tertunda") ?>" ><i class="glyphicon glyphicon-time"></i> &nbsp; Tertunda</a>
				<a class="list-group-item <?php echo ($halaman=='selesai') ? 'active' : '' ?>" href="<?php echo base_url("tiket/selesai") ?>" ><i class="glyphicon glyphicon-check"></i> &nbsp; Selesai</a>
				<a class="list-group-item <?php echo ($halaman=='tidak_selesai') ? 'active' : '' ?>" href="<?php echo base_url("tiket/tidak_selesai") ?>" ><i class="glyphicon glyphicon-remove"></i> &nbsp; Tidak Selesai</a>
			</div>
		</div>

	</div>
</div>
