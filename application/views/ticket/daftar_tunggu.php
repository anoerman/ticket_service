<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
			<div class="panel panel-default">
				<div class="panel-body">
					<h2>Tiket - Daftar Tunggu</h2>
					<div>
						<?php echo $message; ?>
					</div>
					<div class="table-responsive" id="isi">
						<!-- Isi responsif disini -->
					</div>
				</div>
			</div>
		</div>
		<!-- Menu -->
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
			<div class="list-group">
				<a class="list-group-item" href="<?php echo base_url("tiket") ?>" ><i class="glyphicon glyphicon-dashboard"></i> &nbsp; Beranda</a>
				<?php if ($this->ion_auth->logged_in()) :?>
				<a class="list-group-item" href="<?php echo base_url("tiket/tambah") ?>" ><i class="glyphicon glyphicon-plus"></i> &nbsp; Tiket Baru</a>
				<?php endif ?>
				<a class="list-group-item active" href="<?php echo base_url("tiket/daftar_tunggu") ?>" ><i class="glyphicon glyphicon-list"></i> &nbsp; Daftar Tunggu</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/proses") ?>" ><i class="glyphicon glyphicon-refresh"></i> &nbsp; Dalam Proses</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/tertunda") ?>" ><i class="glyphicon glyphicon-time"></i> &nbsp; Tertunda</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/selesai") ?>" ><i class="glyphicon glyphicon-check"></i> &nbsp; Selesai</a>
				<a class="list-group-item" href="<?php echo base_url("tiket/tidak_selesai") ?>" ><i class="glyphicon glyphicon-remove"></i> &nbsp; Tidak Selesai</a>
			</div>
		</div>

	</div>
</div>
