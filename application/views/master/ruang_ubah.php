<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
	<legend>
		Master Ruang 
		&nbsp; <a href="<?php echo base_url("ruang/tambah") ?>" class="btn btn-xs btn-primary">Tambah Baru</a>
	</legend>
	<form action="<?php echo base_url("ruang/ubah") ?>" method="post" autocomplete="off">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo $message ?>
				<?php 
				foreach ($detail->result() as $datas) {
					$id    = $datas->id;
					$nama  = $datas->nama;
					$aktif = $datas->aktif;
				}
				?>
				<div class="form-group">
					<label for="nama" class="control-label">Nama</label>
					<input type="text" name="nama" id="nama" class="form-control" value="<?php echo $nama ?>" required autofocus>
				</div>
				<div class="form-group">
					<label for="nama" class="control-label">Aktif</label>
					<div class="radio">
						<label>
							<input type="radio" name="aktif" id="inputAktif" value="1" <?php echo ($aktif=="1") ? "checked='checked'" : ""; ?>>
							Aktif
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="aktif" id="inputTidakAktif" value="0" <?php echo ($aktif=="0") ? "checked='checked'" : ""; ?>>
							Tidak Aktif
						</label>
					</div>
				</div>
				<div class="form-group">
					<hr>
					<input type="hidden" name="id" id="id" value="<?php echo $id ?>">
					<button type="submit" class="btn btn-primary">Simpan Perubahan</button>
					<a href="<?php echo base_url('ruang'); ?>" class="btn btn-default">Kembali</a>
				</div>
			</div>
		</div>
	</form>
</div>