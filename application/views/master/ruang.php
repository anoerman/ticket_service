<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
	<legend>
		Master Ruang 
		&nbsp; <a href="<?php echo base_url("ruang/tambah") ?>" class="btn btn-xs btn-primary">Tambah Baru</a>
	</legend>
	<?php if (count($detail->result())>0): ?>
		<table class="table table-striped datatables">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Aktif</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			<?php $no = 1;
			foreach ($detail->result() as $datas): ?>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $datas->nama ?></td>
					<td><?php echo ($datas->aktif==1) ? "Ya" : "Tidak" ?></td>
					<td>
						<div class="btn-group btn-group-sm">
							<a href="<?php echo base_url('ruang/ubah/'.$datas->id) ?>" class="btn btn-sm btn-primary">Ubah</a>
							<a href="<?php echo base_url('ruang/hapus/'.$datas->id) ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data ruang : <?php echo $datas->nama ?>')">Hapus</a>
						</div>
					</td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	<?php else: ?>
		<p class="text-center">
			Tidak ada data terdaftar!
		</p>
		<p class="text-center">
			<a href="<?php echo base_url("ruang/tambah") ?>" class="btn btn-primary">Tambah Baru</a>
		</p>
	<?php endif ?>
</div>