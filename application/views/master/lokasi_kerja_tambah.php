<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
	<legend>
		Master Lokasi Kerja 
		&nbsp; <a href="<?php echo base_url("lokasi_kerja/tambah") ?>" class="btn btn-xs btn-primary">Tambah Baru</a>
	</legend>
	<form action="<?php echo base_url("lokasi_kerja/tambah") ?>" method="post" autocomplete="off">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo $message ?>
				<div class="form-group">
					<label for="nama" class="control-label">Nama</label>
					<input type="text" name="nama" id="nama" class="form-control" required autofocus>
				</div>
				<div class="form-group">
					<label for="nama" class="control-label">Aktif</label>
					<div class="radio">
						<label>
							<input type="radio" name="aktif" id="inputAktif" value="1" checked="checked">
							Aktif
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="aktif" id="inputTidakAktif" value="0">
							Tidak Aktif
						</label>
					</div>
				</div>
				<div class="form-group">
					<hr>
					<button type="submit" class="btn btn-primary">Simpan Data</button>
					<a href="<?php echo base_url('lokasi_kerja'); ?>" class="btn btn-default">Kembali</a>
				</div>
			</div>
		</div>
	</form>
</div>