		<!-- Menu -->
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
			<div class="list-group">
				<a class="list-group-item <?php echo ($master_page == "jabatan") ? "active" : ""; ?>" href="<?php echo base_url("jabatan") ?>" ><i class="glyphicon glyphicon-minus"></i> &nbsp; Jabatan</a>
				<a class="list-group-item <?php echo ($master_page == "status_pegawai") ? "active" : ""; ?>" href="<?php echo base_url("status_pegawai") ?>" ><i class="glyphicon glyphicon-minus"></i> &nbsp; Status Pegawai</a>
				<a class="list-group-item <?php echo ($master_page == "unit_kerja") ? "active" : ""; ?>" href="<?php echo base_url("unit_kerja") ?>" ><i class="glyphicon glyphicon-minus"></i> &nbsp; Unit Kerja</a>
				<a class="list-group-item <?php echo ($master_page == "gedung") ? "active" : ""; ?>" href="<?php echo base_url("gedung") ?>" ><i class="glyphicon glyphicon-minus"></i> &nbsp; Gedung</a>
				<a class="list-group-item <?php echo ($master_page == "ruang") ? "active" : ""; ?>" href="<?php echo base_url("ruang") ?>" ><i class="glyphicon glyphicon-minus"></i> &nbsp; Ruang</a>
				<a class="list-group-item <?php echo ($master_page == "lokasi_kerja") ? "active" : ""; ?>" href="<?php echo base_url("lokasi_kerja") ?>" ><i class="glyphicon glyphicon-minus"></i> &nbsp; Lokasi Kerja</a>
				<a class="list-group-item <?php echo ($master_page == "jenis_tiket") ? "active" : ""; ?>" href="<?php echo base_url("jenis_tiket") ?>" ><i class="glyphicon glyphicon-minus"></i> &nbsp; Jenis Tiket</a>
			</div>
		</div>

	</div>
</div>