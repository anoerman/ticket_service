<html>
	<head>
		<title><?php echo $site_title; ?></title>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
	</head>
	<body>
		<div class="container">
			<div class="col-lg-6 col-lg-offset-3">
				<h1><?php echo lang('forgot_password_heading');?></h1>
				<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
				<hr>
				<?php echo $message;?>

				<?php echo form_open("auth/forgot_password");?>
					<div class="form-group">
						<label for="identity" class="control-label">
							<?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?>
						</label>
				      	<?php echo form_input($identity);?>
					</div>
					<div class="form-group">
						<button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url('auth/login'); ?>" class="btn btn-default">Back</a>
					</div>
				<?php echo form_close();?>
			</div>
		</div>
	</body>
</html>
<script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>


<?php /* ?>
<h1><?php echo lang('forgot_password_heading');?></h1>
<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/forgot_password");?>

      <p>
      	<label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	<?php echo form_input($identity);?>
      </p>

      <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

<?php echo form_close();?>
<?php */ ?>