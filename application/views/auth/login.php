<html>
	<head>
		<title><?php echo $site_title; ?></title>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
	</head>
	<body>
		<div class="container">
			<div class="col-lg-6 col-lg-offset-3">
				<h1><?php echo lang('login_heading');?></h1>
				<hr>
				<?php echo $message;?>

				<?php echo form_open("auth/login", array('autocomplete' => 'off'));?>
					<div class="form-group">
						<label for="identity" class="control-label">Username</label>
						<div>
							<input type="text" name="identity" id="identity" class="form-control" value="<?php echo set_value('identity'); ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="identity" class="control-label">Password</label>
						<div>
							<input type="password" name="password" id="password" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label for="remember">
								<input type="checkbox" name="remember" id="remember" value="1">
								Remember me
							</label>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" name="submit" id="submit" class="btn btn-primary">Login</button>
						<a href="<?php echo base_url(); ?>" class="btn btn-default">Homepage</a>
						<br><a href="forgot_password">Forgot your password?</a>
					</div>
				<?php echo form_close();?>
			</div>
		</div>
	</body>
</html>
<script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
