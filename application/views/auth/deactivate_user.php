<div class="container">
	<div class="col-md-12">
		<h1><?php echo lang('deactivate_heading');?></h1>
		<p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>

		<?php echo form_open("auth/deactivate/".$user->id);?>

			<div class="form-group">
				<div class="checkbox">
					<input type="radio" name="confirm" value="yes" checked="checked" />
					<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
				</div>
				<div class="checkbox">
					<input type="radio" name="confirm" value="no" />
					<?php echo lang('deactivate_confirm_n_label', 'confirm');?>
				</div>
			</div>

			<?php echo form_hidden($csrf); ?>
			<?php echo form_hidden(array('id'=>$user->id)); ?>

			<div class="form-group">
				<?php echo form_submit('submit', lang('deactivate_submit_btn'), array('class' => 'btn btn-primary'));?>
				<?php echo anchor('auth', 'Back', array('class' => 'btn btn-default')) ?>
			</div>

		<?php echo form_close();?>
	</div>
</div>