<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
			<h1><?php echo lang('edit_group_heading');?></h1>
			<p><?php echo lang('edit_group_subheading');?></p>

			<div id="infoMessage"><?php echo $message;?></div>

			<?php echo form_open(current_url());?>

				<div class="form-group">
					<?php echo lang('edit_group_name_label', 'group_name');?> <br />
					<?php echo form_input($group_name);?>
				</div>

				<div class="form-group">
					<?php echo lang('edit_group_desc_label', 'description');?> <br />
					<?php echo form_input($group_description);?>
				</div>

				<div class="form-group">
					<?php echo form_submit('submit', lang('edit_group_submit_btn'), array('class' => 'btn btn-primary'));?>
			    	<?php echo anchor('auth', 'Back', array('class' => 'btn btn-default')) ?>
				</div>

			<?php echo form_close();?>
		</div>
	</div>
</div>