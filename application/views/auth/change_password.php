<html>
      <head>
            <title><?php echo $site_title; ?></title>
            <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
      </head>
      <body>
            <div class="container">
                  <div class="col-lg-6 col-lg-offset-3">
                        <h1><?php echo lang('change_password_heading');?></h1>
                        <div id="infoMessage"><?php echo $message;?></div>

                        <?php echo form_open("auth/change_password");?>

                              <p>
                                    <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
                                    <?php echo form_input($old_password);?>
                              </p>

                              <p>
                                    <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
                                    <?php echo form_input($new_password);?>
                              </p>

                              <p>
                                    <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
                                    <?php echo form_input($new_password_confirm);?>
                              </p>

                              <?php echo form_input($user_id);?>
                              <p><?php echo form_submit('submit', lang('change_password_submit_btn'));?></p>

                        <?php echo form_close();?>
                  </div>
            </div>
      </body>
</html>
<script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
