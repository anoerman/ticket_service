<?php /** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
	die('This report should only be run from a Web Browser');

// Include PHPExcel 
require_once '/assets/plugins/phpexcel/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("anoerman")
							 ->setLastModifiedBy("anoerman")
							 ->setTitle("Laporan Tiket")
							 ->setSubject("Laporan Tiket")
							 ->setDescription("Menampilkan laporan tiket ")
							 ->setKeywords("tiket it php codeigniter");

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Tiket IT');

// Header Title
$objPHPExcel->getActiveSheet()->setCellValue("B2", "Laporan Tiket Servis IT");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);

// Height
$objPHPExcel->getActiveSheet()->getRowDimension(4)->setRowHeight(25);

// Width
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);

// Value
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B4', 'No')
            ->setCellValue('C4', 'Jenis Tiket')
            ->setCellValue('D4', 'Tiket Oleh')
            ->setCellValue('E4', 'Tiket Tanggal')
            ->setCellValue('F4', 'Isi Tiket')
            ->setCellValue('G4', 'Status Tiket')
            ->setCellValue('H4', 'Selesai Oleh')
            ->setCellValue('I4', 'Selesai Tanggal')
            ->setCellValue('J4', 'Penanganan Tiket')
            ->setCellValue('K4', 'Status Pegawai')
            ->setCellValue('L4', 'Jabatan')
            ->setCellValue('M4', 'Unit Kerja')
            ->setCellValue('N4', 'Lokasi Kerja')
            ->setCellValue('O4', 'Gedung')
            ->setCellValue('P4', 'Ruang')
            ->setCellValue('Q4', 'Lantai')
            ;

// Header style
$objPHPExcel->getActiveSheet()->getStyle('B4:Q4')->applyFromArray(
	array(
		'fill' => array(
			'type'  => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('argb' => 'ccffcc')
		),
		'borders' => array(
			'top'    => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
			'left'   => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
			'right'  => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
		)
	)
);

// Top Limit
$top_limit = 4;

// Isi
$no = 0;

// Zebra
$zebra = "tidak";

/* Content Here */
foreach ($isi_laporan->result_array() as $dl) {
	$no++;
	$top_limit++;
	
	// Content style
	$objPHPExcel->getActiveSheet()->getStyle('B'.$top_limit.':Q'.$top_limit)->applyFromArray(
		array(
			'borders' => array(
				'top'    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left'   => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
				'right'  => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		)
	);

	// Set zebra color (odds number)
	if ($zebra=="ya") {
		if ($top_limit%2==1) {
			$objPHPExcel->getActiveSheet()->getStyle('B'.$top_limit.':Q'.$top_limit)->applyFromArray(
				array(
					'fill' => array(
					'type'  => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('argb' => 'e1eaea')
					)
				)
			);
		}
	}

	// Content 
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("B".$top_limit, $no)
            ->setCellValue("C".$top_limit, $dl["nama_jenis_tiket"])
            ->setCellValue("D".$top_limit, $dl["fn_oleh"]. " " .$dl["ln_oleh"])
            ->setCellValue("E".$top_limit, date('d M Y', $dl["tanggal"]))
            ->setCellValue("F".$top_limit, $dl["isi"])
            ->setCellValue("G".$top_limit, ucwords($dl["status"]))
            ->setCellValue("H".$top_limit, $dl["fn_selesai_oleh"]. " " .$dl["ln_selesai_oleh"])
            ->setCellValue("J".$top_limit, $dl["penanganan"])
            ->setCellValue("K".$top_limit, $dl["nama_status_pegawai"])
            ->setCellValue("L".$top_limit, $dl["nama_jabatan"])
            ->setCellValue("M".$top_limit, $dl["nama_unit_kerja"])
            ->setCellValue("N".$top_limit, $dl["nama_lokasi_kerja"])
            ->setCellValue("O".$top_limit, $dl["nama_gedung"])
            ->setCellValue("P".$top_limit, $dl["nama_ruang"])
            ->setCellValue("Q".$top_limit, $dl["lantai"])
            ;
            if ($dl['selesai_tanggal']!="") {
	            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("I".$top_limit, date('d M Y', $dl["selesai_tanggal"]));
            }
            else {
	            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("I".$top_limit, "");
            }
}
/* Content Here */

// Wrap it
$objPHPExcel->getActiveSheet()->getStyle('B5:Q'.$top_limit)->getAlignment()->setWrapText(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

ob_clean();
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Laporan Tiket Servis IT '.date('d/m/Y').'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;


?>