<?php /** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Jakarta');

if (PHP_SAPI == 'cli')
	die('This report should only be run from a Web Browser');

// Include PHPExcel 
require_once '/assets/plugins/phpexcel/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("anoerman")
							 ->setLastModifiedBy("anoerman")
							 ->setTitle("Laporan Penyelesaian Tiket Per User")
							 ->setSubject("Laporan Penyelesaian Tiket Per User")
							 ->setDescription("Menampilkan laporan penyelesaian tiket per user")
							 ->setKeywords("tiket it php codeigniter");

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Laporan_Penyelesaian_Tiket');

// Header Title
$objPHPExcel->getActiveSheet()->setCellValue("B2", "Laporan Penyelesaian Tiket Servis IT");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);

// Height
$objPHPExcel->getActiveSheet()->getRowDimension(7)->setRowHeight(25);

// Width
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

// Value
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B7', 'NIP')
            ->setCellValue('C7', 'Nama')
            ->setCellValue('D7', 'Total Tiket')
        ;

// Header style
$objPHPExcel->getActiveSheet()->getStyle('B7:D7')->applyFromArray(
	array(
		'fill' => array(
			'type'  => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('argb' => 'ccffcc')
		),
		'borders' => array(
			'top'    => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
			'left'   => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
			'right'  => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
		)
	)
);

$objPHPExcel->getActiveSheet()->setCellValue("B4", "Tgl Pembuatan:");
$objPHPExcel->getActiveSheet()->setCellValue("C4", date('d F Y', $tanggal_awal) . " - " . date('d F Y', $tanggal_akhir));
$objPHPExcel->getActiveSheet()->setCellValue("B5", "Tgl Pengerjaan:");
$objPHPExcel->getActiveSheet()->setCellValue("C5", date('d F Y', $selesai_tanggal_awal) . " - " . date('d F Y', $selesai_tanggal_akhir));

// Top Limit
$top_limit = 7;

// Isi
$no = 0;

// Zebra
$zebra = "tidak";

/* Content Here */
foreach ($isi_laporan->result_array() as $dl) {
	$no++;
	$top_limit++;
	
	// Content style
	$objPHPExcel->getActiveSheet()->getStyle('B'.$top_limit.':D'.$top_limit)->applyFromArray(
		array(
			'borders' => array(
				'top'    => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left'   => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
				'right'  => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		)
	);

	// Set zebra color (odds number)
	if ($zebra=="ya") {
		if ($top_limit%2==1) {
			$objPHPExcel->getActiveSheet()->getStyle('B'.$top_limit.':D'.$top_limit)->applyFromArray(
				array(
					'fill' => array(
					'type'  => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('argb' => 'e1eaea')
					)
				)
			);
		}
	}

	// Content 
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("B".$top_limit, $dl["username"])
            ->setCellValue("C".$top_limit, $dl["first_name"]. " " .$dl["last_name"])
            ->setCellValue("D".$top_limit, $dl["total"])
            ;
}
/* Content Here */


/* Chart Here */
$objWorksheet = $objPHPExcel->getActiveSheet();
//	Set the Labels for each data series we want to plot
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$dataSeriesLabels1 = array(
	new PHPExcel_Chart_DataSeriesValues('String', 'Laporan_Penyelesaian_Tiket!$D$7', NULL, 1),	//	Total Tiket
);
//	Set the X-Axis Labels
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$xAxisTickValues1 = array(
	new PHPExcel_Chart_DataSeriesValues('String', 'Laporan_Penyelesaian_Tiket!$C$8:$C$'.$top_limit, NULL, $no),	//	Nama
);
//	Set the Data values for each data series we want to plot
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$dataSeriesValues1 = array(
	new PHPExcel_Chart_DataSeriesValues('Number', 'Laporan_Penyelesaian_Tiket!$D$8:$D$'.$top_limit, NULL, $no),
);

//	Build the dataseries
$series1 = new PHPExcel_Chart_DataSeries(
	PHPExcel_Chart_DataSeries::TYPE_PIECHART,				// plotType
	NULL,			                                        // plotGrouping (Pie charts don't have any grouping)
	range(0, count($dataSeriesValues1)-1),					// plotOrder
	$dataSeriesLabels1,										// plotLabel
	$xAxisTickValues1,										// plotCategory
	$dataSeriesValues1										// plotValues
);

//	Set up a layout object for the Pie chart
$layout1 = new PHPExcel_Chart_Layout();
$layout1->setShowVal(TRUE);
$layout1->setShowPercent(TRUE);

//	Set the series in the plot area
$plotArea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
//	Set the chart legend
$legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

$title1 = new PHPExcel_Chart_Title('Statistik Penyelesaian Tiket Per User');


//	Create the chart
$chart1 = new PHPExcel_Chart(
	'chartyo',		// name
	$title1,		// title
	$legend1,		// legend
	$plotArea1,		// plotArea
	true,			// plotVisibleOnly
	0,				// displayBlanksAs
	NULL,			// xAxisLabel
	NULL			// yAxisLabel		- Pie charts don't have a Y-Axis
);

//	Set the position where the chart should appear in the worksheet
$chart1->setTopLeftPosition('F4');
$chart1->setBottomRightPosition('M20');

//	Add the chart to the worksheet
$objWorksheet->addChart($chart1);
/* Chart Here */

// Wrap it
$objPHPExcel->getActiveSheet()->getStyle('B8:D'.$top_limit)->getAlignment()->setWrapText(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

ob_clean();
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Laporan Penyelesaian Tiket Per User Servis IT '.date('d/m/Y').'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->setIncludeCharts(TRUE);
$objWriter->save('php://output');
exit;


?>