		<!-- Menu -->
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2">
			<div class="list-group">
				<a class="list-group-item <?php echo ($laporan_page == "index") ? "active" : ""; ?>" href="<?php echo base_url("laporan") ?>"><i class="glyphicon glyphicon-dashboard"></i> &nbsp; Beranda</a>
				<a class="list-group-item <?php echo ($laporan_page == "tiket") ? "active" : ""; ?>" href="<?php echo base_url("laporan/tiket") ?>"><i class="fa fa-ticket"></i> &nbsp; Laporan Tiket</a>
				<a class="list-group-item <?php echo ($laporan_page == "tiket_per_user") ? "active" : ""; ?>" href="<?php echo base_url("laporan/tiket_per_user") ?>"><i class="fa fa-users"></i> &nbsp; Laporan User</a>
			</div>
		</div>

	</div>
</div>