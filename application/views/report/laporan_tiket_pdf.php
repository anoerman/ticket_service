<?php 
// Require data fpdf
require_once ('assets/plugins/fpdf/fpdf.php');

// Inisialisasi header dan footer PDF
class PDF extends FPDF
{
	var $nama_sistem;
	var $nama_laporan;

	// Set nama sistem
	function set_nama($nama, $tipe)
	{
		$variabel = "nama_".$tipe;
		$this->$variabel = $nama;
	}

	// Page header
	function Header()
	{
		// Logo
		$logo_image = "assets/img/logo.png";
		$this->Image($logo_image,8,8,50);

		// Arial bold 15
		// Move to the right
		// Title
		$this->SetFont('Arial','B',15);
		$this->Cell(45);
		$this->Cell(30,10,$this->nama_sistem,0,1,'L');

		$this->SetFont('Arial','',12);
		$this->Cell(45);
		$this->Cell(30,5, $this->nama_laporan,0,0,'L');
		// Line break
		$this->Ln(10);

		$this->Line(10,29,200,29);
		$this->Line(10,30,200,30);
		// $this->Line(10,30,10,280);
		// $this->Line(200,30,200,280);

	}

	// Page footer
	function Footer()
	{
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		// Page number
		$this->Cell(0,10,'Page '.$this->PageNo().' / {nb}',0,0,'C');
	}
}

// Inisialisasi isi data PDF
$pdf = new PDF('P');
$pdf->set_nama($site_title, "sistem");
$pdf->set_nama("Laporan Tiket", "laporan");
$pdf->AliasNbPages();
$pdf->SetTitle($site_title." Laporan Tiket");
$pdf->SetCreator("anoerman");
$pdf->SetAuthor("anoerman");
$pdf->SetSubject($site_title." Laporan Tiket");
$pdf->SetFont('Arial','',10);

// Tambah halaman
// $pdf->AddPage();

/* Content */
$no = 0;
foreach ($isi_laporan->result_array() as $il) {
	// Page break!
	if (($no % 2) == 0) {
		$pdf->AddPage();
	}

	$no++;

	$pdf->Ln(4);
	$pdf->Cell(40, 8, "Kode :", 0, 0, 'R');
	$pdf->Write(8, $il['kode']);
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Jenis Tiket :", 0, 0, 'R');
	$pdf->Write(8, $il['nama_jenis_tiket']);
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Dibuat Oleh :", 0, 0, 'R');
	$pdf->Write(8, $il['fn_oleh']. " " .$il['ln_oleh']. " (". $il['nama_unit_kerja'] . ")");
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Tanggal :", 0, 0, 'R');
	$pdf->Write(8, date('d F Y', $il['tanggal']));
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Lokasi :", 0, 0, 'R');
	$pdf->Write(8, $il['nama_lokasi_kerja']. " (Gedung: " .$il['nama_gedung']. " , Ruang: " .$il['nama_ruang']. " , Lt " .$il['lantai']. ")");
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Isi Tiket :", 0, 0, 'R');
	$pdf->SetLeftMargin(50);
	$pdf->Write(8, $il['isi']);
	$pdf->SetLeftMargin(10);
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Status :", 0, 0, 'R');
	$pdf->Write(8, ucwords($il['status']));
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Dikerjakan Oleh :", 0, 0, 'R');
	$pdf->Write(8, $il['fn_selesai_oleh']. " " .$il['ln_selesai_oleh']);
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Tanggal :", 0, 0, 'R');
	$pdf->Write(8, date('d F Y', $il['selesai_tanggal']));
	$pdf->Ln(8);
	$pdf->Cell(40, 8, "Penanganan :", 0, 0, 'R');
	$pdf->SetLeftMargin(50);
	$pdf->Write(8, $il['penanganan']);
	$pdf->SetLeftMargin(10);
	$pdf->Ln(8);

	// Page break!
	if (($no % 2) != 0) {
		$pdf->Ln(2);
		$pdf->Cell(190, 0, "", 1, 1);
	}

}
/* Content */


$pdf->Output();

?>