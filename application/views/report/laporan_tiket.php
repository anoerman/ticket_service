<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 pull-right">
	<div class="panel panel-default">
		<div class="panel-body">
			<h2>Laporan Tiket</h2>
			<p>Anda dapat melakukan pembuatan laporan yang bisa disesuaikan dengan kriteria dibawah ini. <!-- Setelah itu anda dapat memilih jenis laporan antara file PDF atau file Excel. --></p>
			<hr>
			<form action="<?php echo base_url('laporan/tiket_hasil'); ?>" method="POST" role="form">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="jenis_tiket" class="control-label">Jenis Tiket :</label>
							<select name="jenis_tiket" id="jenis_tiket" class="form-control">
								<option value="">Semua Jenis Tiket</option>
							<?php foreach ($jenis_tiket->result() as $jenis_tiket): ?>
								<option value="<?php echo $jenis_tiket->id ?>"><?php echo $jenis_tiket->nama ?></option>
							<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label for="oleh" class="control-label">Pembuat Tiket :</label>
							<select name="oleh" id="oleh" class="form-control">
								<option value="">Semua Pembuat Tiket</option>
							<?php foreach ($oleh->result() as $oleh): ?>
								<option value="<?php echo $oleh->username ?>"><?php echo $oleh->first_name. " " .$oleh->last_name ?></option>
							<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label for="selesai_oleh" class="control-label">Pelaksana Tiket :</label>
							<select name="selesai_oleh" id="selesai_oleh" class="form-control">
								<option value="">Semua Pelaksana Tiket</option>
							<?php foreach ($selesai_oleh->result() as $selesai_oleh): ?>
								<option value="<?php echo $selesai_oleh->username ?>"><?php echo $selesai_oleh->first_name. " " .$selesai_oleh->last_name ?></option>
							<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label for="status" class="control-label">Status :</label>
							<select name="status" id="status" class="form-control">
								<option value="">Semua Status</option>
								<option value="daftar tunggu">Daftar Tunggu</option>
								<option value="proses">Dalam Proses</option>
								<option value="tertunda">Tertunda</option>
								<option value="selesai">Selesai</option>
								<option value="tidak selesai">Tidak Selesai</option>
							</select>
						</div>
						<div class="form-group">
							<label for="unit_kerja" class="control-label">Unit Kerja :</label>
							<select name="unit_kerja" id="unit_kerja" class="form-control">
								<option value="">Semua Unit Kerja</option>
							<?php foreach ($unit_kerja->result() as $unit_kerja): ?>
								<option value="<?php echo $unit_kerja->id ?>"><?php echo $unit_kerja->nama ?></option>
							<?php endforeach ?>
							</select>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label for="lokasi_kerja" class="control-label">Lokasi Kerja :</label>
							<select name="lokasi_kerja" id="lokasi_kerja" class="form-control">
								<option value="">Semua Lokasi Kerja</option>
							<?php foreach ($lokasi_kerja->result() as $lokasi_kerja): ?>
								<option value="<?php echo $lokasi_kerja->id ?>"><?php echo $lokasi_kerja->nama ?></option>
							<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label for="gedung" class="control-label">Gedung :</label>
							<select name="gedung" id="gedung" class="form-control">
								<option value="">Semua Gedung</option>
							<?php foreach ($gedung->result() as $gedung): ?>
								<option value="<?php echo $gedung->id ?>"><?php echo $gedung->nama ?></option>
							<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label for="ruang" class="control-label">Ruang :</label>
							<select name="ruang" id="ruang" class="form-control">
								<option value="">Semua Ruang</option>
							<?php foreach ($ruang->result() as $ruang): ?>
								<option value="<?php echo $ruang->id ?>"><?php echo $ruang->nama ?></option>
							<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label for="lantai" class="control-label">Lantai :</label>
							<input type="number" name="lantai" id="lantai" class="form-control">
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><hr></div>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="tanggal_awal" class="control-label">Tanggal Pembuatan :</label>
									<div class="input-group input-daterange">
										<input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control">
										<div class="input-group-addon">s/d</div>
										<input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="selesai_tanggal_awal" class="control-label">Tanggal Penyelesaian :</label>
									<div class="input-group input-daterange">
										<input type="text" name="selesai_tanggal_awal" id="selesai_tanggal_awal" class="form-control">
										<div class="input-group-addon">s/d</div>
										<input type="text" name="selesai_tanggal_akhir" id="selesai_tanggal_akhir" class="form-control">
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
						<hr>
						<input type="submit" name="laporan_pdf" id="laporan_pdf" class="btn btn-danger" value="Generate PDF">
						<input type="submit" name="laporan_excel" id="laporan_excel" class="btn btn-success" value="Generate Excel">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
