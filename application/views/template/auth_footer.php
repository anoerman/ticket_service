<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
		<footer class="footer">
			<div class="container">
			<?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong> | ' : '' ?>
				Page rendered in {elapsed_time} seconds. 
			</div>
		</footer>
	</body>
</html>
<script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- Chosen -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/chosen.min.css') ?>">
<script src="<?php echo base_url('assets/js/chosen.jquery.min.js') ?>"></script>
<script language="javascript">
	$(document).ready(function() {
		$("select").chosen({
			no_results_text: "Data tidak ditemukan!",
			width: "100%"
		});
	});
</script>
<!-- Datetimepicker -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.id.js') ?>"></script>
<script type="text/javascript">
	$('.tanggal').datetimepicker({
		format: "yyyy-mm-dd hh:ii",
		language: "id"
	});
</script>
<!-- Datepicker -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datepicker3.standalone.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datepicker.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datepicker.id.min.js') ?>"></script>
<script type="text/javascript">
	$('.input-daterange').datepicker({
		format: "yyyy-mm-dd",
		weekStart: 1,
		language: "id",
	    daysOfWeekHighlighted: "0,6",
	    todayHighlight: true
	});
</script>
<!-- Datatables -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/datatables.min.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.datatables').DataTable({
			"language" :{
				"url": "assets/js/id_ID.json"
			}
		});
	} );
</script>