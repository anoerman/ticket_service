<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<html>
	<head>
		<title><?php echo $site_title; ?></title>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>">
	</head>
	<body>
		<nav class="navbar navbar-default navbar-static-top" role="navigation">
			<div class="container">
				<a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo $site_title; ?></a>
				<ul class="nav navbar-nav">
					<li <?php echo ($page=="home") ? "class='active'" : ""; ?>>
						<a href="<?php echo base_url(); ?>">Home</a>
					</li>
					<li <?php echo ($page=="tiket") ? "class='active'" : ""; ?>>
						<a href="<?php echo base_url('tiket'); ?>">Tiket</a>
					</li>
				</ul>
				<ul class="nav navbar-nav pull-right">
					<li>
						<a href="<?php echo base_url("auth/login"); ?>">Login</a>
					</li>
				</ul>
			</div>
		</nav>
