<div class="container">
	<div class="col-lg-12">
		<h2><?php echo lang('index_heading');?></h2>

		<div class="row">
			<div class="col-sm-12">
				<div class="btn-group btn-group-sm pull-right">
					<a href="<?php echo base_url('auth/create_user') ?>" class="btn btn-primary">Create New User</a>
					<a href="<?php echo base_url('auth/create_group') ?>" class="btn btn-primary">Create New Group</a>
				</div>
				<p><?php echo lang('index_subheading');?></p>
			</div>
			<div class="clearfix">&nbsp;</div>
		</div>
		
		<?php echo $message;?>
		
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><?php echo lang('index_fname_th');?></th>
						<th><?php echo lang('index_lname_th');?></th>
						<th><?php echo 'Username';?></th>
						<th><?php echo lang('index_email_th');?></th>
						<th><?php echo lang('index_groups_th');?></th>
						<th><?php echo lang('index_status_th');?></th>
						<th><?php echo lang('index_action_th');?></th>
					</tr>
				</thead>
				<?php foreach ($users as $user):?>
					<tr>
			            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->username,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<div class="btn-group-vertical btn-sm">
							<?php foreach ($user->groups as $group):?>
								<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8'), array('class' => 'btn btn-sm btn-default', )) ;?>
			                <?php endforeach?>
							</div>
						</td>
						<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link'), array('class' => 'btn btn-sm btn-success', )) : anchor("auth/activate/". $user->id, lang('index_inactive_link'), array('class' => 'btn btn-sm btn-danger', ));?></td>
						<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit', array('class' => 'btn btn-sm btn-primary', )) ;?></td>
					</tr>
				<?php endforeach;?>
			</table>
		</div>
		
	</div>
</div>