<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*	Laporan Model
*
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*
* Melakukan pengaturan terhadap laporan tiket dari database
* 
*/

class Laporan_model extends CI_Model
{
	/**
	*	Construct
	*
	**/
	public function __construct()
	{
		parent::__construct();
		$this->load->database();

		// Set variabel tabel tiket
		$this->tabel_tiket          = "tiket";
		$this->tabel_tiket_detail   = "tiket_detail";
		$this->tabel_jenis_tiket    = "master_jenis_tiket";
		$this->tabel_user           = "users";
		$this->tabel_jabatan        = "master_jabatan";
		$this->tabel_status_pegawai = "master_status_pegawai";
		$this->tabel_unit_kerja     = "master_unit_kerja";
		$this->tabel_lokasi_kerja   = "master_lokasi_kerja";
		$this->tabel_gedung         = "master_gedung";
		$this->tabel_ruang          = "master_ruang";
	}

	/**
	*	laporan_tiket
	*	Melihat detail tiket berdasarkan kriteria
	*	yang sudah dimasukkan
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.1.0 (2017-03-03)
	*	
	*	@param 		array 		$kriteria
	*	@return 	array 		$datas
	*	
	*/
	public function laporan_tiket($kriteria)
	{
		$init_query = "
			SELECT 
			 ".$this->tabel_tiket.".`id` AS tiket_id, 
			 ".$this->tabel_tiket.".`kode`, 
			 ".$this->tabel_tiket.".`jenis_tiket_id`, 
			 ".$this->tabel_jenis_tiket.".`nama` AS nama_jenis_tiket, 
			 ".$this->tabel_tiket.".`isi`, 
			 ".$this->tabel_tiket.".`oleh`, 
			 u1.`first_name` AS fn_oleh, 
			 u1.`last_name` AS ln_oleh, 
			 ".$this->tabel_tiket.".`tanggal`, 
			 ".$this->tabel_tiket.".`status`, 
			 ".$this->tabel_tiket.".`selesai_oleh`, 
			 u2.`first_name` AS fn_selesai_oleh, 
			 u2.`last_name` AS ln_selesai_oleh, 
			 ".$this->tabel_tiket.".`selesai_tanggal`, 
			 ".$this->tabel_tiket.".`penanganan`, 
			 ".$this->tabel_tiket_detail.".`jabatan_id`, 
			 ".$this->tabel_jabatan.".`nama` AS nama_jabatan, 
			 ".$this->tabel_tiket_detail.".`status_pegawai_id`, 
			 ".$this->tabel_status_pegawai.".`nama` AS nama_status_pegawai, 
			 ".$this->tabel_tiket_detail.".`unit_kerja_id`, 
			 ".$this->tabel_unit_kerja.".`nama` AS nama_unit_kerja, 
			 ".$this->tabel_tiket_detail.".`lokasi_kerja_id`, 
			 ".$this->tabel_lokasi_kerja.".`nama` AS nama_lokasi_kerja, 
			 ".$this->tabel_tiket_detail.".`gedung_id`, 
			 ".$this->tabel_gedung.".`nama` AS nama_gedung, 
			 ".$this->tabel_tiket_detail.".`ruang_id`, 
			 ".$this->tabel_ruang.".`nama` AS nama_ruang, 
			 ".$this->tabel_tiket_detail.".`lantai` 
			FROM ".$this->tabel_tiket." 
			INNER JOIN ".$this->tabel_tiket_detail." ON ".$this->tabel_tiket.".`id` = ".$this->tabel_tiket_detail.".`tiket_id` 
			LEFT JOIN ".$this->tabel_jenis_tiket." ON ".$this->tabel_tiket.".`jenis_tiket_id` = ".$this->tabel_jenis_tiket.".`id` 
			LEFT JOIN ".$this->tabel_jabatan." ON ".$this->tabel_tiket_detail.".`jabatan_id` = ".$this->tabel_jabatan.".`id` 
			LEFT JOIN ".$this->tabel_status_pegawai." ON ".$this->tabel_tiket_detail.".`status_pegawai_id` = ".$this->tabel_status_pegawai.".`id` 
			LEFT JOIN ".$this->tabel_unit_kerja." ON ".$this->tabel_tiket_detail.".`unit_kerja_id` = ".$this->tabel_unit_kerja.".`id` 
			LEFT JOIN ".$this->tabel_lokasi_kerja." ON ".$this->tabel_tiket_detail.".`lokasi_kerja_id` = ".$this->tabel_lokasi_kerja.".`id` 
			LEFT JOIN ".$this->tabel_gedung." ON ".$this->tabel_tiket_detail.".`gedung_id` = ".$this->tabel_gedung.".`id` 
			LEFT JOIN ".$this->tabel_ruang." ON ".$this->tabel_tiket_detail.".`ruang_id` = ".$this->tabel_ruang.".`id` 
			LEFT JOIN ".$this->tabel_user." u1 ON ".$this->tabel_tiket.".`oleh` = u1.`username` 
			LEFT JOIN ".$this->tabel_user." u2 ON ".$this->tabel_tiket.".`selesai_oleh` = u2.`username` 
			WHERE ".$this->tabel_tiket.".`hapus` = '0'
			";

		// Tambah kriteria
		if ($kriteria['jenis_tiket']!="") {
			$init_query .= " AND ".$this->tabel_tiket.".`jenis_tiket_id` = '$kriteria[jenis_tiket]' ";
		}
		if ($kriteria['oleh']!="") {
			$init_query .= " AND ".$this->tabel_tiket.".`oleh` = '$kriteria[oleh]' ";
		}
		if ($kriteria['selesai_oleh']!="") {
			$init_query .= " AND ".$this->tabel_tiket.".`selesai_oleh` = '$kriteria[selesai_oleh]' ";
		}
		if ($kriteria['status']!="") {
			$init_query .= " AND ".$this->tabel_tiket.".`status` = '$kriteria[status]' ";
		}
		if ($kriteria['unit_kerja']!="") {
			$init_query .= " AND ".$this->tabel_tiket_detail.".`unit_kerja_id` = '$kriteria[unit_kerja]' ";
		}
		if ($kriteria['lokasi_kerja']!="") {
			$init_query .= " AND ".$this->tabel_tiket_detail.".`lokasi_kerja_id` = '$kriteria[lokasi_kerja]' ";
		}
		if ($kriteria['gedung']!="") {
			$init_query .= " AND ".$this->tabel_tiket_detail.".`gedung_id` = '$kriteria[gedung]' ";
		}
		if ($kriteria['ruang']!="") {
			$init_query .= " AND ".$this->tabel_tiket_detail.".`ruang_id` = '$kriteria[ruang]' ";
		}
		if ($kriteria['lantai']!="") {
			$init_query .= " AND ".$this->tabel_tiket_detail.".`lantai` = '$kriteria[lantai]' ";
		}
		if ($kriteria['tanggal_awal']!="" && $kriteria['tanggal_akhir']!="") {
			$init_query .= " AND ".$this->tabel_tiket.".`tanggal` >= '$kriteria[tanggal_awal]' ";
			$init_query .= " AND ".$this->tabel_tiket.".`tanggal` <= '$kriteria[tanggal_akhir]' ";
		}
		if ($kriteria['selesai_tanggal_awal']!="" && $kriteria['selesai_tanggal_akhir']!="") {
			$init_query .= " AND ".$this->tabel_tiket.".`selesai_tanggal` >= '$kriteria[selesai_tanggal_awal]' ";
			$init_query .= " AND ".$this->tabel_tiket.".`selesai_tanggal` <= '$kriteria[selesai_tanggal_akhir]' ";
		}

		// Get data
		$datas = $this->db->query($init_query);
		return $datas;
	}

	/**
	*	laporan_tiket_per_user
	*	Melihat penyelesaian tiket berdasarkan kriteria
	*	yang sudah dimasukkan
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-03-06)
	*	
	*	@param 		array 		$kriteria
	*	@return 	array 		$datas
	*	
	*/
	public function laporan_tiket_per_user($kriteria)
	{
		// Ambil pengguna yang menyelesaikan tiket (DISTINCT)
		$init_query = "
			SELECT 
			 DISTINCT(a.selesai_oleh) AS username, 
			 ".$this->tabel_user.".first_name, 
			 ".$this->tabel_user.".last_name, 
			 (SELECT COUNT(id) FROM ".$this->tabel_tiket." WHERE selesai_oleh = a.selesai_oleh) AS total 
			FROM ".$this->tabel_tiket." a 
			LEFT JOIN ".$this->tabel_user." ON a.selesai_oleh = ".$this->tabel_user.".username 
			INNER JOIN ".$this->tabel_tiket_detail." ON a.`id` = ".$this->tabel_tiket_detail.".`tiket_id` 
			WHERE a.hapus = '0' AND a.selesai_oleh != ''  
		";

		// Tambah kriteria
		if ($kriteria['jenis_tiket']!="") {
			$init_query .= " AND a.`jenis_tiket_id` = '$kriteria[jenis_tiket]' ";
		}
		if ($kriteria['oleh']!="") {
			$init_query .= " AND a.`oleh` = '$kriteria[oleh]' ";
		}
		if ($kriteria['status']!="") {
			$init_query .= " AND a.`status` = '$kriteria[status]' ";
		}
		if ($kriteria['unit_kerja']!="") {
			$init_query .= " AND ".$this->tabel_tiket_detail.".`unit_kerja_id` = '$kriteria[unit_kerja]' ";
		}
		if ($kriteria['tanggal_awal']!="" && $kriteria['tanggal_akhir']!="") {
			$init_query .= " AND a.`tanggal` >= '$kriteria[tanggal_awal]' ";
			$init_query .= " AND a.`tanggal` <= '$kriteria[tanggal_akhir]' ";
		}
		if ($kriteria['selesai_tanggal_awal']!="" && $kriteria['selesai_tanggal_akhir']!="") {
			$init_query .= " AND a.`selesai_tanggal` >= '$kriteria[selesai_tanggal_awal]' ";
			$init_query .= " AND a.`selesai_tanggal` <= '$kriteria[selesai_tanggal_akhir]' ";
		}

		// Get data
		$datas = $this->db->query($init_query);
		return $datas;
	}

}

/* Enf of laporan_model */