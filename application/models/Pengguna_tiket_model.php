<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*	Pengguna Tiket Model
*
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*
* Melakukan pengaturan terhadap data pengguna dari database
* 
*/

class Pengguna_tiket_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	*	jabatan
	*	Mengembalikan daftar jabatan / data 1 jabatan jika
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@param 		string 		$aktif
	*	@return 	array 		$datas
	*	
	**/
	public function jabatan($id = null, $aktif = null)
	{
		$this->tabel = "master_jabatan";
		if ( $id !== null ) {
			$this->db->where("id", $id);
		}
		// jika ada info aktif - cek aktif
		if ($aktif == null) {
			$this->db->where("aktif", '1');
		}
		$this->db->where("hapus", '0');
		$this->db->order_by("aktif", "desc");
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	gedung
	*	Mengembalikan daftar gedung / data 1 gedung jika
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@param 		string 		$aktif
	*	@return 	array 		$datas
	*	
	**/
	public function gedung($id = null, $aktif = null)
	{
		$this->tabel = "master_gedung";
		if ( $id !== null ) {
			$this->db->where("id", $id);
		}
		// jika ada info aktif - cek aktif
		if ($aktif == null) {
			$this->db->where("aktif", '1');
		}
		$this->db->where("hapus", '0');
		$this->db->order_by("aktif", "desc");
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	ruang
	*	Mengembalikan daftar ruang / data 1 ruang jika
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@param 		string 		$aktif
	*	@return 	array 		$datas
	*	
	**/
	public function ruang($id = null, $aktif = null)
	{
		$this->tabel = "master_ruang";
		if ( $id !== null ) {
			$this->db->where("id", $id);
		}
		// jika ada info aktif - cek aktif
		if ($aktif == null) {
			$this->db->where("aktif", '1');
		}
		$this->db->where("hapus", '0');
		$this->db->order_by("aktif", "desc");
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	status_pegawai
	*	Mengembalikan daftar status_pegawai / data 1 status_pegawai jika
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@param 		string 		$aktif
	*	@return 	array 		$datas
	*	
	**/
	public function status_pegawai($id = null, $aktif = null)
	{
		$this->tabel = "master_status_pegawai";
		if ( $id !== null ) {
			$this->db->where("id", $id);
		}
		// jika ada info aktif - cek aktif
		if ($aktif == null) {
			$this->db->where("aktif", '1');
		}
		$this->db->where("hapus", '0');
		$this->db->order_by("aktif", "desc");
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	unit_kerja
	*	Mengembalikan daftar unit_kerja / data 1 unit_kerja jika
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@param 		string 		$aktif
	*	@return 	array 		$datas
	*	
	**/
	public function unit_kerja($id = null, $aktif = null)
	{
		$this->tabel = "master_unit_kerja";
		if ( $id !== null ) {
			$this->db->where("id", $id);
		}
		// jika ada info aktif - cek aktif
		if ($aktif == null) {
			$this->db->where("aktif", '1');
		}
		$this->db->where("hapus", '0');
		$this->db->order_by("aktif", "desc");
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	lokasi_kerja
	*	Mengembalikan daftar lokasi_kerja / data 1 lokasi_kerja jika
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@param 		string 		$aktif
	*	@return 	array 		$datas
	*	
	**/
	public function lokasi_kerja($id = null, $aktif = null)
	{
		$this->tabel = "master_lokasi_kerja";
		if ( $id !== null ) {
			$this->db->where("id", $id);
		}
		// jika ada info aktif - cek aktif
		if ($aktif == null) {
			$this->db->where("aktif", '1');
		}
		$this->db->where("hapus", '0');
		$this->db->order_by("aktif", "desc");
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	ambil_users_jabatan
	*	Mengembalikan data 1 jabatan sesuai ID yang diminta
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@return 	array 		$datas
	*	
	**/
	public function ambil_users_jabatan($id)
	{
		$this->tabel = "users_jabatan";
		$this->tabel2 = "master_jabatan";
		$this->db->where("user_id", $id);
		$this->db->join($this->tabel2, $this->tabel.'.jabatan_id = '. $this->tabel2 .'.id');
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	ambil_users_status_pegawai
	*	Mengembalikan data 1 status_pegawai sesuai ID yang diminta
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@return 	array 		$datas
	*	
	**/
	public function ambil_users_status_pegawai($id)
	{
		$this->tabel = "users_status_pegawai";
		$this->tabel2 = "master_status_pegawai";
		$this->db->where("user_id", $id);
		$this->db->join($this->tabel2, $this->tabel.'.status_pegawai_id = '. $this->tabel2 .'.id');
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	ambil_users_unit_kerja
	*	Mengembalikan data 1 unit_kerja sesuai ID yang diminta
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@return 	array 		$datas
	*	
	**/
	public function ambil_users_unit_kerja($id)
	{
		$this->tabel = "users_unit_kerja";
		$this->tabel2 = "master_unit_kerja";
		$this->db->where("user_id", $id);
		$this->db->join($this->tabel2, $this->tabel.'.unit_kerja_id = '. $this->tabel2 .'.id');
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	ambil_users_lokasi_kerja
	*	Mengembalikan data 1 lokasi_kerja sesuai ID yang diminta
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@return 	array 		$datas
	*	
	**/
	public function ambil_users_lokasi_kerja($id)
	{
		$this->tabel = "users_lokasi_kerja";
		$this->tabel2 = "master_lokasi_kerja";
		$this->db->where("user_id", $id);
		$this->db->join($this->tabel2, $this->tabel.'.lokasi_kerja_id = '. $this->tabel2 .'.id');
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	ambil_users_gedung
	*	Mengembalikan data 1 gedung sesuai ID yang diminta
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@return 	array 		$datas
	*	
	**/
	public function ambil_users_gedung($id)
	{
		$this->tabel = "users_gedung";
		$this->tabel2 = "master_gedung";
		$this->db->where("user_id", $id);
		$this->db->join($this->tabel2, $this->tabel.'.gedung_id = '. $this->tabel2 .'.id');
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	ambil_users_ruang
	*	Mengembalikan data 1 ruang sesuai ID yang diminta
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@return 	array 		$datas
	*	
	**/
	public function ambil_users_ruang($id)
	{
		$this->tabel = "users_ruang";
		$this->tabel2 = "master_ruang";
		$this->db->where("user_id", $id);
		$this->db->join($this->tabel2, $this->tabel.'.ruang_id = '. $this->tabel2 .'.id');
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	ambil_users_lantai
	*	Mengembalikan data 1 lantai sesuai ID yang diminta
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$id
	*	@return 	array 		$datas
	*	
	**/
	public function ambil_users_lantai($id)
	{
		$this->tabel = "users_lantai";
		$this->db->where("user_id", $id);
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	/**
	*	simpan_users_jabatan
	*	Menyimpan data kunci untuk users dan jabatan
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$jabatan_id
	*	@return 	bool
	*	
	*/
	public function simpan_users_jabatan($user_id, $jabatan_id)
	{
		$this->tabel = "users_jabatan";
		$data["user_id"] = $user_id;
		$data["jabatan_id"] = $jabatan_id;
		if ($this->db->insert($this->tabel, $data)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	simpan_users_status_pegawai
	*	Menyimpan data kunci untuk users dan status_pegawai
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$status_pegawai_id
	*	@return 	bool
	*	
	*/
	public function simpan_users_status_pegawai($user_id, $status_pegawai_id)
	{
		$this->tabel = "users_status_pegawai";
		$data["user_id"] = $user_id;
		$data["status_pegawai_id"] = $status_pegawai_id;
		if ($this->db->insert($this->tabel, $data)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	simpan_users_unit_kerja
	*	Menyimpan data kunci untuk users dan unit_kerja
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$unit_kerja_id
	*	@return 	bool
	*	
	*/
	public function simpan_users_unit_kerja($user_id, $unit_kerja_id)
	{
		$this->tabel = "users_unit_kerja";
		$data["user_id"] = $user_id;
		$data["unit_kerja_id"] = $unit_kerja_id;
		if ($this->db->insert($this->tabel, $data)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	simpan_users_lokasi_kerja
	*	Menyimpan data kunci untuk users dan lokasi_kerja
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$lokasi_kerja_id
	*	@return 	bool
	*	
	*/
	public function simpan_users_lokasi_kerja($user_id, $lokasi_kerja_id)
	{
		$this->tabel = "users_lokasi_kerja";
		$data["user_id"] = $user_id;
		$data["lokasi_kerja_id"] = $lokasi_kerja_id;
		if ($this->db->insert($this->tabel, $data)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	simpan_users_gedung
	*	Menyimpan data kunci untuk users dan gedung
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$gedung_id
	*	@return 	bool
	*	
	*/
	public function simpan_users_gedung($user_id, $gedung_id)
	{
		$this->tabel = "users_gedung";
		$data["user_id"] = $user_id;
		$data["gedung_id"] = $gedung_id;
		if ($this->db->insert($this->tabel, $data)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	simpan_users_ruang
	*	Menyimpan data kunci untuk users dan ruang
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$ruang_id
	*	@return 	bool
	*	
	*/
	public function simpan_users_ruang($user_id, $ruang_id)
	{
		$this->tabel = "users_ruang";
		$data["user_id"] = $user_id;
		$data["ruang_id"] = $ruang_id;
		if ($this->db->insert($this->tabel, $data)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	simpan_users_lantai
	*	Menyimpan data untuk users dan lantai
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$lantai
	*	@return 	bool
	*	
	*/
	public function simpan_users_lantai($user_id, $lantai)
	{
		$this->tabel = "users_lantai";
		$data["user_id"] = $user_id;
		$data["lantai"]  = $lantai;
		if ($this->db->insert($this->tabel, $data)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	ubah_users_jabatan
	*	ubah data detail jabatan dengan data baru
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$jabatan_id
	*	@return 	bool
	*	
	*/
	public function ubah_users_jabatan($user_id, $jabatan_id)
	{
		$this->tabel = "users_jabatan";
		$data["jabatan_id"] = $jabatan_id;
		if ($this->db->update($this->tabel, $data, array('user_id' => $user_id))) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	ubah_users_status_pegawai
	*	ubah data detail status_pegawai dengan data baru
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$status_pegawai_id
	*	@return 	bool
	*	
	*/
	public function ubah_users_status_pegawai($user_id, $status_pegawai_id)
	{
		$this->tabel = "users_status_pegawai";
		$data["status_pegawai_id"] = $status_pegawai_id;
		if ($this->db->update($this->tabel, $data, array('user_id' => $user_id))) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	ubah_users_unit_kerja
	*	ubah data detail unit_kerja dengan data baru
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$unit_kerja_id
	*	@return 	bool
	*	
	*/
	public function ubah_users_unit_kerja($user_id, $unit_kerja_id)
	{
		$this->tabel = "users_unit_kerja";
		$data["unit_kerja_id"] = $unit_kerja_id;
		if ($this->db->update($this->tabel, $data, array('user_id' => $user_id))) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	ubah_users_lokasi_kerja
	*	ubah data detail lokasi_kerja dengan data baru
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$lokasi_kerja_id
	*	@return 	bool
	*	
	*/
	public function ubah_users_lokasi_kerja($user_id, $lokasi_kerja_id)
	{
		$this->tabel = "users_lokasi_kerja";
		$data["lokasi_kerja_id"] = $lokasi_kerja_id;
		if ($this->db->update($this->tabel, $data, array('user_id' => $user_id))) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	ubah_users_gedung
	*	ubah data detail gedung dengan data baru
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$gedung_id
	*	@return 	bool
	*	
	*/
	public function ubah_users_gedung($user_id, $gedung_id)
	{
		$this->tabel = "users_gedung";
		$data["gedung_id"] = $gedung_id;
		if ($this->db->update($this->tabel, $data, array('user_id' => $user_id))) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	ubah_users_ruang
	*	ubah data detail ruang dengan data baru
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$ruang_id
	*	@return 	bool
	*	
	*/
	public function ubah_users_ruang($user_id, $ruang_id)
	{
		$this->tabel = "users_ruang";
		$data["ruang_id"] = $ruang_id;
		if ($this->db->update($this->tabel, $data, array('user_id' => $user_id))) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	ubah_users_lantai
	*	ubah data detail lantai dengan data baru
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-16)
	*	
	*	@param 		string 		$user_id
	*	@param 		string 		$lantai
	*	@return 	bool
	*	
	*/
	public function ubah_users_lantai($user_id, $lantai)
	{
		$this->tabel = "users_lantai";
		$data["lantai"] = $lantai;
		if ($this->db->update($this->tabel, $data, array('user_id' => $user_id))) {
			return true;
		}
		else {
			return false;
		}
	}


}