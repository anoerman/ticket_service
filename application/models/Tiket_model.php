<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*	Tiket Model
*
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*
* Melakukan pengaturan terhadap data tiket dari database
* 
*/

class Tiket_model extends CI_Model
{

	/**
	*	Construct
	*
	**/
	public function __construct()
	{
		parent::__construct();
		$this->load->database();

		// Set variabel tabel tiket
		$this->tabel_tiket          = "tiket";
		$this->tabel_tiket_detail   = "tiket_detail";
		$this->tabel_jenis_tiket    = "master_jenis_tiket";
		$this->tabel_user           = "users";
		$this->tabel_jabatan        = "master_jabatan";
		$this->tabel_status_pegawai = "master_status_pegawai";
		$this->tabel_unit_kerja     = "master_unit_kerja";
		$this->tabel_lokasi_kerja   = "master_lokasi_kerja";
		$this->tabel_gedung         = "master_gedung";
		$this->tabel_ruang          = "master_ruang";
	}

	/**
	*	jenis_tiket
	*	Mengembalikan daftar jenis_tiket / data 1 jenis_tiket jika
	*	ID disediakan sebagai parameter
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-17)
	*	
	*	@param 		string 		$id
	*	@param 		string 		$aktif
	*	@return 	array 		$datas
	*	
	**/
	public function jenis_tiket($id = null, $aktif = null)
	{
		if ( $id !== null ) {
			$this->db->where("id", $id);
		}
		// jika ada info aktif - cek aktif
		if ($aktif == null) {
			$this->db->where("aktif", '1');
		}
		$this->db->where("hapus", '0');
		$this->db->order_by("aktif", "desc");
		$datas = $this->db->get($this->tabel_jenis_tiket);
		
		return $datas;
	}

	/**
	*	simpan
	*	Menyimpan data tiket yang sudah diisikan pada formulir
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-18)
	*	
	*	@param 		array 		$datas
	*	@return 	bool
	*	
	*/
	public function simpan($datas)
	{
		if ($this->db->insert($this->tabel_tiket, $datas)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	simpan_detail
	*	Menyimpan data tiket yang sudah diisikan pada formulir
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-18)
	*	
	*	@param 		array 		$datas
	*	@return 	bool
	*	
	*/
	public function simpan_detail($datas)
	{
		if ($this->db->insert($this->tabel_tiket_detail, $datas)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	*	ubah
	*	Menyimpan data tiket yang sudah diisikan pada formulir
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-27)
	*	
	*	@param 		array 		$datas
	*	@param 		string 		$id
	*	@return 	bool
	*	
	*/
	public function ubah($datas, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->tabel_tiket, $datas);
		
		return true;
	}

	/**
	*	ubah_detail
	*	Menyimpan data tiket yang sudah diisikan pada formulir
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-27)
	*	
	*	@param 		array 		$datas
	*	@param 		string 		$tiket_id
	*	@return 	bool
	*	
	*/
	public function ubah_detail($datas, $tiket_id)
	{
		$this->db->where('tiket_id', $tiket_id);
		$this->db->update($this->tabel_tiket_detail, $datas);
		
		return true;
	}

	/**
	*	hapus
	*	Menghapus data tiket yang sudah diisikan pada formulir
	*	Dalam kasus ini mengubah status hapus menjadi 1.
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-27)
	*	
	*	@param 		array 		$datas
	*	@param 		string 		$id
	*	@return 	bool
	*	
	*/
	public function hapus($datas, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->tabel_tiket, $datas);
		
		return true;
	}

	/**
	*	detail_tiket
	*	Melihat satu tiket berdasarkan id tiket
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-22)
	*	
	*	@param 		string 		$tiket_id
	*	@return 	array 		$datas
	*	
	*/
	public function detail_tiket($tiket_id)
	{
		$datas = $this->db->query("
			SELECT 
			 tiket.`id` AS tiket_id, 
			 tiket.`kode`, 
			 tiket.`jenis_tiket_id`, 
			 master_jenis_tiket.`nama` AS nama_jenis_tiket, 
			 tiket.`isi`, 
			 tiket.`oleh`, 
			 u1.`first_name` AS fn_oleh, 
			 u1.`last_name` AS ln_oleh, 
			 tiket.`tanggal`, 
			 tiket.`status`, 
			 tiket.`selesai_oleh`, 
			 u2.`first_name` AS fn_selesai_oleh, 
			 u2.`last_name` AS ln_selesai_oleh, 
			 tiket.`selesai_tanggal`, 
			 tiket.`penanganan`, 
			 tiket_detail.`jabatan_id`, 
			 master_jabatan.`nama` AS nama_jabatan, 
			 tiket_detail.`status_pegawai_id`, 
			 master_status_pegawai.`nama` AS nama_status_pegawai, 
			 tiket_detail.`unit_kerja_id`, 
			 master_unit_kerja.`nama` AS nama_unit_kerja, 
			 tiket_detail.`lokasi_kerja_id`, 
			 master_lokasi_kerja.`nama` AS nama_lokasi_kerja, 
			 tiket_detail.`gedung_id`, 
			 master_gedung.`nama` AS nama_gedung, 
			 tiket_detail.`ruang_id`, 
			 master_ruang.`nama` AS nama_ruang, 
			 tiket_detail.`lantai` 
			FROM tiket 
			INNER JOIN tiket_detail ON tiket.`id` = tiket_detail.`tiket_id` 
			LEFT JOIN master_jenis_tiket ON tiket.`jenis_tiket_id` = master_jenis_tiket.`id` 
			LEFT JOIN master_jabatan ON tiket_detail.`jabatan_id` = master_jabatan.`id` 
			LEFT JOIN master_status_pegawai ON tiket_detail.`status_pegawai_id` = master_status_pegawai.`id` 
			LEFT JOIN master_unit_kerja ON tiket_detail.`unit_kerja_id` = master_unit_kerja.`id` 
			LEFT JOIN master_lokasi_kerja ON tiket_detail.`lokasi_kerja_id` = master_lokasi_kerja.`id` 
			LEFT JOIN master_gedung ON tiket_detail.`gedung_id` = master_gedung.`id` 
			LEFT JOIN master_ruang ON tiket_detail.`ruang_id` = master_ruang.`id` 
			LEFT JOIN users u1 ON tiket.`oleh` = u1.`username` 
			LEFT JOIN users u2 ON tiket.`selesai_oleh` = u2.`username` 
			WHERE tiket.`id` = '$tiket_id'
		");
		return $datas;
	}

	/**
	*	ambil_tiket
	*	Melihat data tiket yang sudah ada di database
	*	berdasarkan status tiket dan status hapusnya
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-20)
	*	
	*	@param 		string 		$status
	*	@param 		string 		$hapus
	*	@return 	array 		$datas
	*	
	*/
	public function ambil_tiket($status, $hapus = null, $limit = null, $start = null)
	{
		if ( $hapus == null ) {
			$this->db->where($this->tabel_tiket.".hapus", "0");
		}
		$this->db->where($this->tabel_tiket.".status", $status);
		$this->db->order_by($this->tabel_tiket . ".id", "desc");
		$this->db->join($this->tabel_tiket_detail, $this->tabel_tiket.'.id = '. $this->tabel_tiket_detail .'.tiket_id');
		$this->db->join($this->tabel_jenis_tiket, $this->tabel_tiket.'.jenis_tiket_id = '. $this->tabel_jenis_tiket .'.id');
		$this->db->join($this->tabel_user, $this->tabel_tiket.'.oleh = '. $this->tabel_user .'.username');
		
		if ($limit !== null) {
			$this->db->limit($limit, $start);
		}

		$datas = $this->db->get($this->tabel_tiket);
		return $datas;
	}

	/**
	*	hitung_tiket
	*	Melakukan penghitungan data tiket yang ada di database
	*	berdasarkan status tiket dan status hapusnya
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-28)
	*	
	*	@param 		string 		$status
	*	@param 		string 		$hapus
	*	@return 	array 		$datas
	*	
	*/
	public function hitung_tiket($status = null, $hapus = null)
	{
		if ($status !== null) {
			$this->db->where('status', $status);
		}
		if ($hapus == null) {
			$this->db->where('hapus', '0');
		}
			
		$this->db->from($this->tabel_tiket);
		return $this->db->count_all_results();
	}

	/**
	*	hitung_tiket_per_tanggal
	*	Melakukan penghitungan data tiket yang ada di database
	*	berdasarkan tanggal tiket dan status hapusnya
	*	
	*	@author 	Noerman Agustiyan
	*	@version 	1.0.0 (2017-02-28)
	*	
	*	@param 		string 		$tanggal
	*	@param 		string 		$hapus
	*	@return 	array 		$datas
	*	
	*/
	public function hitung_tiket_per_tanggal($tanggal = null, $hapus = null)
	{
		// get awal tanggal s/d akhir
		$patokan = date('Y-m-d', $tanggal);
		$awal    = strtotime($patokan . " 00:00:01");
		$akhir   = strtotime($patokan . " 23:59:59");

		if ($tanggal !== null) {
			$this->db->where('tanggal >=', $awal);
			$this->db->where('tanggal <=', $akhir);
		}
		if ($hapus == null) {
			$this->db->where('hapus', '0');
		}
			
		$this->db->from($this->tabel_tiket);
		return $this->db->count_all_results();
	}


	
}

/* End of Tiket_model.php */