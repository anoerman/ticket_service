<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*	Status_pegawai Model
*
*	@author 	Noerman Agustiyan
* 				noerman.agustiyan@gmail.com
*				@anoerman
*	
*	@link 		https://github.com/anoerman
*		 		https://gitlab.com/anoerman
*
* Melakukan pengaturan terhadap master Status_pegawai di database
* 
*/

class Status_pegawai_model extends CI_Model
{

	/**
	 * Construct
	 *
	 **/
	public function __construct()
	{
		parent::__construct();
		$this->load->database();

		$this->tabel = "master_status_pegawai";
	}

	// Daftar
	public function daftar($id = null)
	{
		if ( $id !== null ) {
			$this->db->where("id", $id);
		}
		$this->db->where("hapus", '0');
		$this->db->order_by("aktif", "desc");
		$datas = $this->db->get($this->tabel);
		
		return $datas;
	}

	// Simpan
	public function simpan($datas)
	{
		$data['id']         = "";
		$data['nama']       = $datas['nama'];
		$data['aktif']      = $datas['aktif'];
		$data['hapus']      = '0';
		$data['created_by'] = $this->session->userdata('identity');
		$data['created_on'] = time();
		$data['updated_by'] = $this->session->userdata('identity');
		$data['updated_on'] = time();

		$this->db->insert($this->tabel, $data);

		return TRUE;
	}

	// Ubah
	public function ubah($datas)
	{
		$id                 = $datas['id'];
		$data['nama']       = $datas['nama'];
		$data['aktif']      = $datas['aktif'];
		$data['hapus']      = '0';
		$data['updated_by'] = $this->session->userdata('identity');
		$data['updated_on'] = time();

		$this->db->update($this->tabel, $data, array('id' => $id));

		return TRUE;
	}

	// Hapus
	public function hapus($id)
	{
		$data['hapus']      = '1';
		$data['updated_by'] = $this->session->userdata('identity');
		$data['updated_on'] = time();

		$this->db->update($this->tabel, $data, array('id' => $id));

		return TRUE;
	}
}